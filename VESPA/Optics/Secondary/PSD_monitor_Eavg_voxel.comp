/*******************************************************************************
*
* McStas, neutron ray-tracing package
*         Copyright 1997-2002, All rights reserved
*         Risoe National Laboratory, Roskilde, Denmark
*         Institut Laue Langevin, Grenoble, France
*
* ComponenE: PSD_monitor_E
*
* %I
* Written by: Peter Willendrup, derived from PSD_e_monitor by Kim Lefmann
* Date: Feb 3, 1998
* Origin: Risoe
* Modified by M.Zanetti to tally E instead of t, 13.10.2018
*
*
* Position-sensitive monitor with a E signal pr. bin.
*
* %D
* An (nx times ny) pixel PSD monitor with nE time bins pr pixel.
*
* Will output 1 inEegrated PSD images plus an nE time bin E signal pr pixel.
*
* Example: PSD_e_monitor_TOF(xmin=-0.1, xmax=0.1, ymin=-0.1, ymax=0.1, nx=90, ny=90, Emin=1, Emax=1000, nE=1000, filename="Output")
*
* %P
* INPUT PARAMETERS:
*
* xmin: [m]             Lower x bound of detector opening 
* xmax: [m]             Upper x bound of detector opening 
* ymin: [m]             Lower y bound of detector opening 
* ymax: [m]             Upper y bound of detector opening 
* Emin: [mu-s]          Lower energy bound 
* Emax: [mu-s]          Upper energy bound 
* xwidth: [m]           Width/diameter of detector (x). Overrides xmin, xmax 
* yheight: [m]          Height of detector (y). Overrides ymin, ymax 
* nx: [1]               Number of pixel columns 
* ny: [1]               Number of pixel rows 
* nE: [1]               Number of E bins 
* filename: [string]    Name of file in which to store the detector image 
* restore_neutron: [1]  If set, the monitor does not influence the neutron state 
*
* OUTPUT PARAMETERS:
*
* PSD_e_N: []             Array of neutron counts
* PSD_e_p: []             Array of neutron weight counts
* PSD_e_p2: []            Array of second moments
*
* %E
*******************************************************************************/


DEFINE COMPONENT PSD_monitor_Eavg_voxel
DEFINITION PARAMETERS (nx=90, ny=90, string filename, restore_neutron=0)
SETTING PARAMETERS (xmin=0, xmax=0, ymin=0, ymax=0, xwidth=0, yheight=0,zthick=0, Emin=0, Emax=0)
OUTPUT PARAMETERS (PSD_e_N, PSD_e_p, PSD_e_p2, PSD_e_Ns, PSD_e_ps, PSD_e_p2s)

DECLARE
%{
// double PSD_e_N[nx][ny][nE];
// double PSD_e_p[nx][ny][nE];
// double PSD_e_p2[nx][ny][nE];
double PSD_e_Vs[nx][ny];
double PSD_e_V2s[nx][ny];
double PSD_e_V3s[nx][ny];
double PSD_e_V4s[nx][ny];
double PSD_e_Ns[nx][ny];
double PSD_e_ps[nx][ny];
double PSD_e_p2s[nx][ny];
 double PSD_e_p4s[nx][ny];
double  PSD_e_NPRCs[nx][ny];
double DeltaTtubo, tcyl1,tcyl2;
// double ctrX;
// double DeltaTtubo, tcyl1,tcyl2;

%}

INITIALIZE
%{
int i,j,k;





if (xwidth  > 0) { xmax = xwidth/2;  xmin = -xmax; }
    if (yheight > 0) { ymax = yheight/2; ymin = -ymax; }

    if ((xmin >= xmax) || (ymin >= ymax)) {
            printf("PSD_e_E_monitor: %s: Null detection area !\n"
                   "ERROR        (xwidth,yheight,xmin,xmax,ymin,ymax). Exiting",
           NAME_CURRENT_COMP);
      exit(0);
    }

    if (Emin >= Emax) {
      printf("PSD_e_E_monitor: %s: Unmeaningful E definition!\n",
	     NAME_CURRENT_COMP);
      exit(0);
    }

    for (i=0; i<nx; i++)
      for (j=0; j<ny; j++) {
	PSD_e_Ns[i][j] = 0;
	PSD_e_ps[i][j] = 0;
	PSD_e_p2s[i][j] = 0;
   PSD_e_p4s[i][j] = 0;
   PSD_e_NPRCs[i][j]=0;
// 	for (k=0; k<nE; k++) {
// 	  PSD_e_N[i][j][k] = 0;
// 	  PSD_e_p[i][j][k] = 0;
// 	  PSD_e_p2[i][j][k] = 0;
// 	}
      }
    %}
TRACE
  %{
    int i,j,k,interS;
    double E,t0,mut;



//     PROP_Z0;
    
    
   
      t0=0;
//    PROP_Z0;
//    mut=t;   
   // check intersection with detector box
   interS = box_intersect(&tcyl1,&tcyl2, x, y, z, vx, vy, vz,xwidth, yheight, zthick);  
    DeltaTtubo=fabs(tcyl2-tcyl1);
    mut=tcyl1+DeltaTtubo/2.0;   
   
//           MPI_Comm_rank(MPI_COMM_WORLD, &myrankD);
          
//             if(myrankD==0){
//                printf("\n x y z %lf %lf %lf %lf %d",x, y, z,mut*1e6,interS); 
//                }    
      
//     if (x>xmin && x<xmax && y>ymin && y<ymax && (mut)>tmin && (mut)<tmax)
E=VS2E*(vx*vx+vy*vy+vz*vz);
    if (interS)// && (E)>Emin && (E)<Emax )
    {
//       DeltaTtubo=fabs(tcyl2-tcyl1);
       DeltaTtubo=(rand01()-0.5)*DeltaTtubo*0.999; 
//        RESTORE_NEUTRON(INDEX_CURRENT_COMP, x, y, z, vx, vy, vz, t, sx, sy, sz, p);
//       ALLOW_BACKPROP;
      PROP_DT(mut+DeltaTtubo);

      
    
      i = floor((x - xmin)*nx/(xmax - xmin));
      j = floor((y - ymin)*ny/(ymax - ymin));
//       k = floor((E - Emin)*nE/(Emax - Emin));

//       PSD_e_N[i][j][k]++;
//       PSD_e_p[i][j][k] += p;
//       PSD_e_p2[i][j][k] += p*p;

//       ctrX=((double)i)*xwidth/((double)nx)+xmin-xwidth/((double)nx)/2.0;         
//       DeltaTtubo=0;
//       if(cylinder_intersect( &tcyl1,&tcyl2, x-ctrX, y, z, vx, vy, vz, xwidth/((double)nx)/2.0 , yheight) ){
//       DeltaTtubo=tcyl2-tcyl1;  /// travelling time through detector
//       DeltaTtubo=rand01()*DeltaTtubo;   } //else {ABSORB;}
    



      PSD_e_Ns[i][j]++;
      
      PSD_e_ps[i][j] += p;
      PSD_e_Vs[i][j] += E*p;         /// weighted average of E
      
      PSD_e_p2s[i][j] += p;// *p;        /// suqred to compute Ex(x^2)
      PSD_e_V2s[i][j] += E*E*p;// *p; 
      
       PSD_e_p4s[i][j] = 1;
//        PSD_e_V4s[i][j] = 1;   ///squared gives the variance

      
      PSD_e_NPRCs[i][j]=1;


      SCATTER;
    }
    else
    {ABSORB;}
    if (restore_neutron) {
      RESTORE_NEUTRON(INDEX_CURRENT_COMP, x, y, z, vx, vy, vz, t, sx, sy, sz, p);
    }
%}

SAVE
%{
  int kk,ll,i,j,ncore;
  char ff[256];
  char tt[256];
  sprintf(ff, "%s_Eavg",filename);
  ncore=1;
#ifdef USE_MPI
ncore=mpi_node_count;

/*map nodes that really contribute*/  
/*each node should do it because NPRC is not shared - will be there cumulative effects?*/
    mc_MPI_Sum(&PSD_e_NPRCs[0][0], nx*ny*1);
    // it gives a 1 maps if        PSD_e_Vs=1 and then    PSD_e_Vs/  PSD_e_NPRC
//     MPI_Barrier(MPI_COMM_WORLD);

#endif


  for (i=0; i<nx; i++)
      for (j=0; j<ny; j++) {

             if(PSD_e_Ns[i][j]>0){
            	PSD_e_Vs[i][j]  = PSD_e_Vs[i][j]/ PSD_e_ps[i][j];
              PSD_e_V2s[i][j] = PSD_e_V2s[i][j]/ PSD_e_p2s[i][j];;                     


             }
  }
  
  /// SECOND: standard deviation of energy
  for (i=0; i<nx; i++)
      for (j=0; j<ny; j++) {

             if(PSD_e_Ns[i][j]>0){
            
              PSD_e_V3s[i][j] = sqrt(fabs(PSD_e_V2s[i][j]-(PSD_e_Vs[i][j])*(PSD_e_Vs[i][j])));                     
              PSD_e_V4s[i][j] = fabs(PSD_e_V2s[i][j]-PSD_e_Vs[i][j]*PSD_e_Vs[i][j]);       // Expected(sigma^2) is calculated only n times so is equal to sigma^2
                                                                                            

             }
  }


  //correct by number of mpi 
    for (i=0; i<nx; i++)
      for (j=0; j<ny; j++) {

             if(PSD_e_Ns[i][j]>0){
            	PSD_e_Vs[i][j]  = PSD_e_Vs[i][j]/(PSD_e_NPRCs[i][j]);
              PSD_e_V2s[i][j] = PSD_e_V2s[i][j]/(PSD_e_NPRCs[i][j]);                     
              PSD_e_V3s[i][j] = PSD_e_V3s[i][j]/(PSD_e_NPRCs[i][j]);  
              PSD_e_V4s[i][j] = PSD_e_V4s[i][j]/(PSD_e_NPRCs[i][j]);  

             }
  }

  
  
  
  DETECTOR_OUT_2D(
	"PSD monitor E avg",
        "X position [cm]",
        "Y position [cm]",
        xmin*100.0, xmax*100.0, ymin*100.0, ymax*100.0,
        nx, ny,
         &PSD_e_Ns[0][0],&PSD_e_Vs[0][0],&PSD_e_V2s[0][0],
        ff);






  sprintf(ff, "%s_stdev",filename);
  DETECTOR_OUT_2D(
	"PSD monitor E stdev",
        "X position [cm]",
        "Y position [cm]",
        xmin*100.0, xmax*100.0, ymin*100.0, ymax*100.0,
        nx, ny,
         &PSD_e_p4s[0][0],&PSD_e_V3s[0][0],&PSD_e_V4s[0][0],
        ff);

//   for (kk=0; kk<nx; kk++) {
//     for (ll=0; ll<ny; ll++) {
//       sprintf(ff, "%s_%i_%i",filename,kk,ll);
//       sprintf(tt, "PSD monitor E signal in bin %i,%i (%g,%g) m ",kk,ll,xmin+kk*(xmax-xmin)/nx,ymin+ll*(ymax-ymin)/ny);
//       DETECTOR_OUT_1D(
// 		      tt,
// 		      "Energy [meV]",
// 		      "Intensity",
// 		      "E", Emin, Emax, nE,
// 		      &PSD_e_N[kk][ll][0],&PSD_e_p[kk][ll][0],&PSD_e_p2[kk][ll][0],
// 		      ff);
//     }
//   }
%}

MCDISPLAY
%{
  magnify("xy");
  multiline(5, (double)xmin, (double)ymin, 0.0,
               (double)xmax, (double)ymin, 0.0,
               (double)xmax, (double)ymax, 0.0,
               (double)xmin, (double)ymax, 0.0,
               (double)xmin, (double)ymin, 0.0);
%}

END
