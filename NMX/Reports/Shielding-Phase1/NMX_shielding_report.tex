\title{Shielding of LoKI:\\ Interim Technical Report}
\date{\today}

\documentclass[12pt,a4]{essarticle}
\usepackage{graphicx}
\usepackage{subcaption}
\usepackage{amsmath}
\usepackage{booktabs}
\usepackage{array} 
\usepackage{url}
\usepackage{color}
\usepackage[section]{placeins}
\usepackage{fancyhdr}


\usepackage{geometry}
 \geometry{
 a4paper,
 total={210mm,297mm},
 left=30mm,
 right=20mm,
 top=25mm,
 bottom=20mm,
 }

\renewcommand*{\rmdefault}{phv} % font
\renewcommand{\sfdefault}{phv}

\begin{document}

\author{Nataliia Cherkashyna} 
\reviewer{Phillip M. Bentley}
\version{1}
%
%\begin{centering}
% 
%\end{centering}
%
%\begin{centering}
%Neutron Optics and Shielding Group \\ 
%\end{centering}
%
%\begin{centering}
%European Spallation Source ESS AB \\  
%\end{centering}

\maketitle

\section{Introduction}

The European Spallation Source combines a powerful spallation source with the instrumental goal of being able to measure weak processes with cold neutrons.  Whereas existing spallation sources are either short pulse (microsecond) or continuous, the ESS will be a ``long pulse'' source, of the order of a few milliseconds, with a low repetition rate.  Despite the low repetition rate, an optimisation of the long pulse instrumentation favours measurements across frame boundaries - i.e. when the next proton pulse produces neutrons (neutrons from several pulses are present in the instrument) - on approximately 70$\%$ of the instrument suite. The data spike that can be detected as the next pulse arrives is a high energy physics phenomenon that has been called a "prompt pulse", because those neutrons possess such high energy that they appear at virtually zero time-of-flight as measured by the neutron instruments. The "prompt-pulse" has been observed since the first days of neutron spallation sources. The issue about high energy background and the technical challenges, related to it, has been known for a while \cite{russel}. \\

The high energy background does reduce the functionality of the neutron instruments, by increasing the noise-to-signal ratio. Typically, it causes a degradation in performance by forcing the instruments to discard a region in time-of-flight where the high energy background spike occurs. The spike is systematic - it is time correlated, and it is not a random error that reduces in relative size compared to the measured signal.  Attempts to subtract this background have proven to be unsatisfactory. \\

A prime candidate mechanism in the prompt pulse is the phenomenon of particle showers. These are the cascades of secondary particles produced by high energy particle interactions with dense matter. There are two different mechanisms of particle showers: electromagnetic and hadronic \cite{gaudio}. Electromagnetic showers are caused by photons and electrons. Electrons create many events in matter by ionization and bremsstrahlung, and photons are able to penetrate  quite far through material before they lose energy. The second type, hadronic showers, is caused by hadrons,  and strong nuclear forces are involved into those interactions. The hadronic showers are characterized by ionization and interactions between incident particles and nuclei of the material. Hadronic showers are complex, as many reactions take place and many different particles are produced, and it is essential to use sophisticated modelling packages such as GEANT4 \cite{volk} to understand hadronic showers. \\

In \cite{cherkashyna2013} an overview on how the ESS is aiming to reduce the backgrounds on the instruments was given. Different materials were investigated in terms of their high energy shielding properties and some new shielding solutions were presented, with particular emphasis on reducing the signal from high energy background.  \\

The following report contains the description of shielding work on LoKI - A broad-band SANS instrument. Simulations were performed to estimate high energy background at the sample position using conceptual model of the instrument. \\

It should be borne in mind that this work was originally planned for \emph{PHASE 2}, and after launching the activity in \emph{PHASE 1} only half of the required effort has been invested into the work so far.

\section{Simulation tools}
Energy spectrum of the neutrons that was used for simulations of the instrument shielding was calculated by Konstantin Batkov (Neutronics Group, Target Division, ESS) using MCNP (general purpose Monte Carlo n-particle transport code) \cite{mcnp}. The technical details of the target station used to perform the calculations could be found in ESS Technical Design Report \cite{TDR}. The energy spectrum shown on the figure  \ref{fig: ESS source}, is the full energy spectrum. In the simulations of LoKI shielding concept only high-energy part of they spectrum was used, i.e. neutrons withe energy equal to and above 1 $eV$. 

 \begin{figure}[h]
 \centering
   	\includegraphics[width=12 cm]{figs/ESS_MCNP.pdf}
	\caption[ESS source]{Energy spectrum of the neutrons that are coming into the guide, ``ESS source''.}
	\label{fig: ESS source}
 \end{figure}
 	
Geant4, a reliable simulation toolkit for areas where particle interactions in matter play a main role \cite{agostinelli}, was used to perform the simulations. That toolkit is a well-known main simulation engine for high energy physics applications. Different materials were investigated in order to understand their shielding properties against high energy particles. \\

The physics list $QGSP$\_ $BERT$\_ $HP$, recommended for shielding applications in the energy range of interest, was chosen for the simulations. This is the set of physical processes which describe the interaction of particles with dense matter \cite{agostinelli}. The abbreviation means the following: $QGS$ is for quark-gluon string model, $P$ means precompound,  $BERT$ means that Bertini intra-nuclear cascade model is used, and $HP$ states that high precision neutron tracking model is also included \cite{speckmayer}. 
 
\section{Simulations setup. Conceptual design geometry and materials}

 \begin{figure}[t!]
 \centering
   	\includegraphics[width=14 cm]{./figs/Slide2.png}
	\caption[Layout_setup]{Layout of the LoKI instrument, simulation setup}
	\label{fig: Layout of the LoKI instrument, simulation setup}
 \end{figure}
 
 
On the figures \ref{fig: Layout of the LoKI instrument, simulation setup} and \ref{fig: Layout of the LoKI instrument, materials} the general layout of LoKI  instrument, which includes dimensions of different parts and positions of the key parts of the instrument, is shown. On the figure \ref{fig: Layout of the LoKI instrument, simulation setup} the details of simulation setup are shown. On the figure \ref{fig: Layout of the LoKI instrument, materials} details of simulation setup are given with respect to materials used in the simulation. In the basic conceptual model of LoKI where the goal was to estimate the background signal at the sample position instrument cave and detector tank are not present, as well as some details like choppers and apertures. Those changes and other details of the simulations are discussed below.
The particle source in Geant4 simulations (``ESS source'', mentioned above) is placed at the entrance of the guide. Neutrons were emitted along the $Z$-axis, the size of the source is $12 cm$ in length and  $3.5 cm$ in height. ``Length'' and ``height'' are dimensions along $X$-axis and $Y$-axis respectively. Dimension along $Z$-axis will be called ``thickness''. \\
 
Length and height of two big blocks in that geometrical setup - iron block and concrete block, are equal to 4 m. Target monolith is represented by block of iron that is 4 m thick, bunker shielding and shielding outside the bunker are represented by ordinary concrete block which is 14.5 m thick. The source of the particles is situated in the middle of target monolith block right next to guide entrance. Inside the target monolith curved guide is placed ($R =$ 57.7 m), that guide is made of copper. There are 8 detectors placed in the various locations inside the concrete block, makes by ``$D1$'', ``$D2$'', etc. These objects which cover $XY$ surfaces of the block at various locations are 0.1 mm wide are a perfect sensitive volumes; they are made of vacuum. They record information about each particle that is coming to the volume such as particle type, kinetic energy, position and momentum. \\


Between $D1$ and $D2$ there is a straight guide, made of aluminium, surrounded by empty (i.e. filled with air) space reserved for choppers, height and with of that space are 80 cm and 100 cm, thickness is equal to 1.5 m. \\

  \begin{figure}[h!]
 \centering
   	\includegraphics[width=14 cm]{./figs/Slide3.png}
	\caption[Layout_materials]{Layout of the LoKI instrument, materials}
	\label{fig: Layout of the LoKI instrument, materials}
 \end{figure}
 
Between $D2$ and $D3$ heavy collimator block is placed. It is a tungsten cylinder diameter of which is $50 cm$ and it's thickness equals to 2.2 m. Tungsten has been chosen to check what is the maximum effect of the first heavy collimator block in background reduction. Shielding properties of tungsten as well as other materials are discussed below. Inside that block the curved guide made of copper is placed ($R =$ 32 m). An $S$-bender guide option was considered in the simulations. \\

Between $D3$ and $D4$ there is a space, identical to the space between $D1$ and $D2$, thickness of which is equal to 2.8 m. This is the place for choppers as well as for the secondary shutter, which is currently not present in that simulation setup. Inside that space straight guide made of aluminium is placed.\\

Between $D4$ and $D5$ there is a collimator changer made of brass (63\% of copper, 37\% of zinc). That collimator changer is a cylinder diameter of which equals to 50 cm and it's thickness equals to 2 m. In the beginning the collimator changer is surrounded by copper collimator block which is 1 m in length, height and thickness. Copper was used as a material for collimator blocks because it has smaller number of energy windows it the cross-section in comparison, for example, with iron. Shielding properties of copper and other materials are discussed below. Inside the collimator changer straight guide is placed, 1 m of which is made of copper and remaining 1 m is made of BK-7 optical glass (borkron glass) \cite{borkron}. The composition of that material used in simulations is the following composition:  45\% of oxygen, 30\%  of silicon, 15\% of boron, 5\% of sodium, 5\% of potassium. \\

Between $D5$ and $D6$ there is a collimator changer made of aluminium. That collimator changer is a cylinder which is 50 cm and it's thickness equals to 3 m. The end of the collimator changer is surrounded by copper collimator block which is 1 m in length, height and thickness. Inside the collimator block there is a straight guide, first 2 m of which are made of borkron glass. and last 1 m is made of copper. \\

Between $D6$ and $D7$ there is a collimator changer made of brass. That collimator changer is a cylinder which is 50 cm and its thickness is equal to 3 m. Inside the collimator block there is a straight guide made of borkron glass. \\

Between $D7$ and $D8$, i.e. between the end of the concrete shielding and the sample position there is 2 m iron tube, inner diameter of which is 4.23 cm and outer diameter is 6.23 cm. Sensitive volume $D8$ is placed at the sample position.



\section{Shielding properties of different materials}
The shielding properties of various materials have been investigated by ESS and its partners over the last few years.  Here we show results in order to understand their shielding properties against energy spectrum shown on figure \ref{fig: ESS source} to check what are the differences in attenuating ESS neutron energy spectra. Materials studied here include iron, copper, brass, regular concrete, lead and  tungsten. They were investigated in terms of their high energy shielding properties. The simulations involved blocks of each material of 50 $m$ by 50 $m$, with different thicknesses. On the figure \ref{fig: Materials} the result of the simulations for the blocks which are 50 $cm$ thick.  There is a sensitive volume placed behind the block which records information about particles ``survived'' after they went through the block. On the figure \ref{fig: Materials} the energy spectra of out coming neutrons are shown. The particle source in Geant4 simulations (particle gun) is placed in front of the centre of the block. $2 \times \mathrm{{10}^{6}}$ neutrons with ESS source energy distribution were emitted.  \\

\begin{figure}[htbp]
   \begin{subfigure}[b]{0.32\textwidth}
	 \subcaption[Iron]{50 cm of iron}
   	\includegraphics[width=5 cm]{./figs/Iron_50cm_front.png}
	\label{fig: 50 cm of iron}
 \end{subfigure}
  \begin{subfigure}[b]{0.32\textwidth}
  	\subcaption[Copper]{50 cm of copper}
   	\includegraphics[width=5 cm]{./figs/Copper_50cm_front.png}
	\label{fig: 50 cm of copper}
 \end{subfigure} 
 \begin{subfigure}[b]{0.32\textwidth}
	\subcaption[Brass]{50 cm of brass}  	
  	\includegraphics[width=5 cm]{./figs/Brass_50cm_front.png}
	\label{fig: 50 cm of brass}
 \end{subfigure}
    \begin{subfigure}[b]{0.32\textwidth}
	 \subcaption[Concrete]{50 cm of concrete}
   	\includegraphics[width=5 cm]{./figs/Concrete_50cm_front.png}
	\label{fig: 50 cm of concrete}
 \end{subfigure}
  \begin{subfigure}[b]{0.32\textwidth}
  	\subcaption[Lead]{50 cm of lead}
   	\includegraphics[width=5 cm]{./figs/Lead_50cm_front.png}
	\label{fig: 50 cm of lead)}
 \end{subfigure} 
 \begin{subfigure}[b]{0.32\textwidth}
	\subcaption[Tungsten]{50 cm of tungsten}  	
  	\includegraphics[width=5 cm]{./figs/Tungsten_50cm_front.png}
	\label{fig: 50 cm of tungsten}
 \end{subfigure}
\caption{Energy spectra of neutrons that are detected behind of the blocks of different materials after $2 \times \mathrm{{10}^{6}}$ neutrons with ESS source energy distribution were emitted into the blocks. Units of energy on the graphs (a)-(f) are  log10(Energy/MeV).  Please note that the graphs are displayed with different scaling factors on the $y$-axis when comparing the different materials.}
\label{fig: Materials}
\end{figure}
As is clear from figure \ref{fig: Materials}, different materials demonstrate different ESS spectrum attenuation features. For example, energy spectrum of the neutrons which came out of the block of copper (figure \ref{fig: 50 cm of copper}) has only one major peak when the same spectrum of block of iron (figure \ref{fig: 50 cm of iron}) has many peaks at different energies due to the windows in the cross-sections. In the case of brass (figure \ref{fig: 50 cm of brass}), as an example of mixture of two different materials there are much less neutrons that ``survived'' because of the different windows in the cross-sections of copper and zinc. That illustrates the advantages of mixed materials and layers of different materials for shielding purposes. On the figure \ref{fig: 50 cm of tungsten} energy spectrum of neutrons which came out of the block of tungsten is shown. The number of neutrons is from one to two orders of magnitude smaller in all energy ranges compared to other materials. That is the reason why tungsten was choose to check the maximum effect of first heavy collimator block for the conceptual shielding design model. Copper also has attractive shielding characteristics, and copper-based materials such as brass, or laminates with material combinations, will be used at the key points of the instrument shielding in the final design. 

\section{Results of the simulations. \\
Heavy collimator block vs. no heavy collimator block}
In the conceptual design geometry heavy collimator block is present in the set of simulations discussed in that report. Apart from the general calculations of the background an the sample position there was one more investigation performed to estimate the influence of heavy first collimator block placed between sensitive volumes $D2$ and $D3$ and ``no heavy first collimator case'' when there is no heavy first collimator block in the model and curved guide between $D2$ and $D3$ is placed in the concrete block. On the figure \ref{fig: Detector8_comparison} neutron energy spectra detected at the sample position are shown for those two cases.

 \begin{figure}[h]
 \centering
   	\includegraphics[width = 6 cm]{./figs/Detector8_comparison.png}
	\caption[Detector8_comparison]{Neutron spectrum at the sample position, detected in sensitive volume $D8$. \\ Curve $1$ corresponds to the ``no heavy first collimator case'' - geometry of conceptual design does not include heavy collimator block between sensitive volumes $D2$ and $D3$. Curve $2$ corresponds to the case when the heavy collimator block is present.}
	\label{fig: Detector8_comparison}
 \end{figure}

As it is demonstrated on the figure \ref{fig: Detector8_comparison} first heavy collimator reduces background at the sample position by 1 order of magnitude in the energy range below 10 $MeV$, and by factor of 5 in the energy range from 10 $MeV$ to 100 $MeV$. 

 \section{Results of the simulations. \\
 Background signal at the sample position}
The main simulation was performed with $N_0 = 9.28\times 10^{21}$ neutrons.  To normalise that value per time unit the macro-pulse current value, equals to 50 $mA$  according to ESS Technical Design Report \cite{TDR}. Number of protons produced ($N_p = 31.2\times 10^{17}$ $protons/s$), number of neutrons per proton for the spectrum that was used $1.07$, size of the LoKI guide entrance (3.5 $cm$ by 3.5 $cm$), and also number of the particles that is coming into LoKI guide entrance, $C_{LoKI} = 6.9\times 10^{14}$ $neutrons/s$, were taken into account for the calculation of background signal at the sample position.

The number of neutrons detected in sensitive volume $D8$ at the sample position is $N_8 = 5247$.  Therefore, the number of background neutrons normalised per number of incident particles and area of the detector is:\\
\begin{equation}
 B_8 = \frac{N_8}{N_0}  C_{LoKI} \frac{1}{16} = 2.43 \times 10^{-5} neutrons/m^{2}/s
\end{equation}

 \begin{figure}[h]
 \centering
   	\includegraphics[width = 6 cm]{./figs/Detector8_spectrum.png}
	\caption[Detector8_spectrum]{Neutron spectrum at the sample position, detected in sensitive volume $D8$, plotted as relative intensity (arb units) vs energy.}
	\label{fig: Detector8_spectrum}
 \end{figure}

Value of the background discussed above and turned to be low, is the preliminary result obtained from a perfect conceptual model. Improving model and changing the details of instrument geometry (for example, gaps between the concrete blocks, pipes, apertures, neighbouring beamlines, etc) will cause the significant background rise. 

\section{Estimate of Cost}

The bunker forms the common shielding shared between the instruments up to 15 m from the target center. At 10 m, twice out of line-of-sight is also achieved and light-beam shielding consisting of approximately 100 cm of concrete can be used for the 3 m of shielding outside of the bunker and up to the position of the cave.  Three heavy collimators will be used within the bunker to stop fast neutrons streaming down the guide. They are initially designed as 100 $\times$ 100 $\times$ 100 cm$^3$ copper.  The exact composition will be optimized at a later stage.  The cave will consist of steel cans, filled with wax, based on the ISIS-TS2 cave concept.

Estimation of the shielding costs for LOKI \\

Based on the above shielding concept, an estimation of the shielding costs can be carried out. The basis for the costs are provided in \cite{cost-benefit} and described in detail there. The cost breakdown is shown in table \ref{tab:costBreakdown}.

\begin{table}[htpb]
\centering
\begin{tabular}{lc}
\toprule

{\bf Item}		& {\bf Cost (kEuro)} \\
\midrule
\midrule

Cave &  1500 \\
Heavy collimator &  50$\times 3$\\
External bunker shielding & 15 \\
Secondary shutter & 40 \\
\midrule

Total & 1705  \\

\bottomrule
\end{tabular}
\caption{Breakdown to key shielding components for LOKI project.}
\label{tab:costBreakdown}
\end{table}


\section{Future plans}
Work on shielding on LoKI instrument discussed in that report was based on conceptual shielding design. Next stage of shielding work on LoKI will include: 
\begin{itemize}
\item{The simulations currently run to the sample position.  Realistic models of the cave and detectors will be merged into the geometry.}
\item{Improvement of variance reduction method used.}
\item{More details will be added to the geometry of the simulation setup to make the background estimates more accurate as and when the details become available (e.g. pipework, conduits).}
\item{Cost-benefit study of materials and material combinations for key parts of the shielding will be done.}
\end{itemize}


\bibliographystyle{unsrt}
\bibliography{Shielding_report_11_11_14}


\end{document}
