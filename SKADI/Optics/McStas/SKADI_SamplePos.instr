/*******************************************************************************
*
* McStas, neutron ray-tracing package
*         Copyright 1997-2002, All rights reserved
*         Risoe National Laboratory, Roskilde, Denmark
*         Institut Laue Langevin, Grenoble, France
*
* Instrument: SKADI Standard design after final bunker design
*
* Sebastian Jaksch
*
*
* INPUT PARAMETERS:
* 
*******************************************************************************/
DEFINE INSTRUMENT SKADI (NGblen=0.03,                  /*width of the neutron guide blinds*/
                                /*ScPlus=0.07,*/
                                Clen = 20.0,                 /*length of the collimation*/
                                SampD=0.01,                 /*Diameter of sample*/
                                bendL1=4.49,                 /*length of upstream bender part of the initial S-bender*/
                                bendL2=4.49,                 /*length of downstream bender part of the initial S-bender*/
                                bendR1 = 84.0,             /*bending radius of upstream bender part of the initial S-bender*/
                                bendR2=84.0,                 /*bending radius of downstream bender part of the initial S-bender*/
                                bendM = 3.0,                /*m-value at the concave side of the S-bender*/
                                bendiM = 3.0,               /*m-value at the convex side of the S-bender*/
                                dfltM = 3.0,                /*default m-value throughout the instrument*/
                                //MDOWN = 3.6,                /*m-value of the bottom side of the polarizing supermirror*/
                                //Min=1.5,                    /*m-value of the neutron guide surrounding the polarizing supermirror*/
                                //cnum=0.00,                  /*switch for the polarizer (switched on for cnum>0.5)*/
                                //ROT=0.0,                    /*rotation of the instrument*/
                                modnum= 11.0,               /*select a sample type, see description of SANS_benchmark2 component*/
                                incs=0.0005,                /*the incoherent background from the overall sample (cm-1), should read ca. 1.0 for water, 0.5 for half D2O, half H2O, and ca. 0.02 for D2O*/
                                npulses = 4.0,              /*number of pulses to be simulated*/
                                ChopperSlit=1,               /*Slits in chopper disks*/
                                colWidth=0.02,        /*width of the collimations slits in x and y direction*/
        D1=0.02152,      /*Delay times for choppers 1-4*/  
        D2=0.03174,
        D3=0.03653,
        D4=0.03845
        )

/* keep default values as much as possible */

DECLARE
%{
  #ifndef M_PI
  #define M_PI 3.14159265358979323846
  #endif
  int32_t flag;
  double lambdamin,lambdamax;
  char SANSareaName[256];
  char MonShortName[256];
  char MonLongName[256];
  char numchar[10];
  double currentPos=0;
%}
INITIALIZE
%{
  lambdamin=0.2;
  lambdamax=70;
  sprintf(SANSareaName,"SANSarea_det_Chopper_");
  sprintf(MonShortName,"MonitorShortDistance_Chopper_");
  sprintf(MonLongName,"MonitorLongDistance_Chopper_");
  //sprintf(numchar,"%g",ChopperSwitch);
  strncat(SANSareaName,numchar,256);
  strncat(MonShortName,numchar,256);
  strncat(MonLongName,numchar,256);
  /*number of slits*/
  strncat(MonShortName,".txt",256);
  strncat(MonLongName,".txt",256);
%}
TRACE

COMPONENT origin = Arm()
  AT (0,0,0) ABSOLUTE

/**********************************************
currentPos variable to define the current position
for absolute distances with respect to the source
**********************************************/

//ESS Source with physically feasible parameters for opening etc.
COMPONENT source=ESS_moderator(sourcedef="2014", Lmin=lambdamin, Lmax=lambdamax, dist=2.0,focus_xw=NGblen, focus_yh=NGblen, xwidth_c=0.23, xwidth_t=0.12,extraction_opening=120)
  AT (0, 0, 0) RELATIVE origin

COMPONENT rspin  = Spin_random ()
  AT (0, 0, 2.0001) RELATIVE source

//currentPos=2.0001;


/************************************************
Bender component
************************************************/
COMPONENT pbar   = Progress_bar(percent=2)
  AT (0, 0, 2.0002) RELATIVE source

/************************************************
This monitor is to check the input of the moderator
before the neutrons enter the bender
************************************************/
//COMPONENT SourceMonitor=Monitor_Radius  (xmin = -.5*NGblen, xmax = 0.5*NGblen, ymin = -.5*NGblen, ymax = 0.5*NGblen,options  = "hdiv bins=100 limits[-2.5 2.5]; lambda bins=50 limits[1.0 20.0]", filename = "MonSource.psd", num=ChopperSlit)
//AT(0, 0, 2.00025) RELATIVE source
/*currentPos=2.00025*/


COMPONENT Bender1 = Bender(w=NGblen, h=NGblen, r=bendR1, Win=bendL1/bendR1,/*number of channels*/k=5,/*thickness of blades*/ d=0.000675,
                                R0a=0.99,Qca=0.02174,alphaa=6.07,ma=3, Wa=0.003,
                                R0i=0.99,Qci=0.02174,alphai=6.07,mi=3,Wi=0.003,
                                R0s=0.99,Qcs=0.02174,alphas=6.07,ms=3, Ws=0.003)
AT (0, 0, 2.0003) RELATIVE source
ROTATED (0,0,90)  RELATIVE source
/*currentPos=2.0003+bendL1(4.49)=6.4903*/


COMPONENT Bender2 = Bender(w=NGblen, h=NGblen, r=bendR2, Win=bendL2/bendR2,/*number of channels*/k=5,/*thickness of blades*/d=0.0005,
                                R0a=0.99,Qca=0.02174,alphaa=6.07,ma=3, Wa=0.003,
                                R0i=0.99,Qci=0.02174,alphai=6.07,mi=3,Wi=0.003,
                                R0s=0.99,Qcs=0.02174,alphas=6.07,ms=3, Ws=0.003)
AT (0, 0, 6.4903+0.001) RELATIVE source
ROTATED (0,0,270)  RELATIVE source
/*currentPos=6.4903+bendL2(4.49)+0.001=10.9813*/

/**********************************************
Monitor Component at the Exit of the S-Bender
**********************************************/
//COMPONENT BenderMonitor=Monitor_Radius  (xmin = -.5*NGblen, xmax = 0.5*NGblen, ymin = -.5*NGblen, ymax = 0.5*NGblen,options  = "hdiv bins=100 limits[-2.5 2.5]; lambda bins=50 limits[1.0 20.0]", filename = "MonSBender.psd", num=ChopperSlit)
//AT(0, 0, /*currentPos=*/10.9813+0.0001) RELATIVE source
//currentPos(10.9814)


/**********************************************
Chopper Positions
Chopper description    Position/m    Frequency/Hz  Opening time/s  Opening Angle/deg
Frame Overlap      15.5      14    0.01957    98.64
High order supp.    22.85      14    0.02885    145.41
High Res 1      26.30      Nx7    0.03321    83.685
High Res 1      26.6      Nx7    0.03359    84.635


Choppers can be either be before or after the initial 
collimation aperture colap, depending on the 
collimation length. The foreseen collimation
lengths Clen are
8,14 and 20 m.

Each setting is switched on or off by a WHEN
statement in the corresponding components.
**********************************************/

/**********************************************
Monitor Component at the entry to the Chopper
section
**********************************************/
//COMPONENT ChopperEntryMonitor=Monitor_Radius  (xmin = -.5*NGblen, xmax = 0.5*NGblen, ymin = -.5*NGblen, ymax = 0.5*NGblen,options  = "hdiv bins=100 limits[-2.5 2.5]; lambda bins=50 limits[1.0 20.0]", filename = "MonEntryChopper.psd", num=ChopperSlit)
//AT(0, 0, /*curretnPos=*/10.991) RELATIVE source

/**********************************************
20 m  Collimation setting
(colap is at 16 m from the source)

Clen == 20
**********************************************/

COMPONENT guide20col_1 = Guide_gravity(w1=NGblen, h1=NGblen, w2=NGblen, h2=NGblen, l=4.49, R0=0.99, Qc=0.02174, alpha=6.07, m=dfltM, W=0.003)
WHEN((Clen == 20))
AT(0, 0, 11.0) RELATIVE source
//currentPos(11.0)+=4.49=15.49

COMPONENT chopper20_1 = DiskChopper(radius=0.3,yheight=0.07, theta_0=98.64/ChopperSlit, nu=14/ChopperSlit, nslit=ChopperSlit, delay=D1/ChopperSlit ,isfirst=0)
WHEN(Clen == 20)
AT(0, 0, 15.5) RELATIVE source
//currentPos=15.5

COMPONENT guide20col_2 = Guide_gravity(w1=NGblen, h1=NGblen, w2=NGblen, h2=NGblen, l=0.48, R0=0.99, Qc=0.02174, alpha=6.07, m=dfltM, W=0.003)
WHEN((Clen == 20))
AT(0, 0, 15.51) RELATIVE source
//currentPos(15.51)+=0.48=15.99

/**********************************************
Collimation Aperture for 20 m
spatial restraint, has to be at 16 m. (Bunker wall at 15 m, Chopper at 15.5m)
**********************************************/
COMPONENT colap_20 = Slit(xmin= -colWidth/2, xmax=colWidth/2, ymin = -colWidth/2, ymax=colWidth/2)
WHEN(Clen == 20)
AT(0,0,16.0) RELATIVE source
//currentPos=16.0m

COMPONENT chopper20_2 = DiskChopper(radius=0.3,yheight=0.07, theta_0=145.41/ChopperSlit, nu=14/ChopperSlit, nslit=ChopperSlit, delay=D2/ChopperSlit,isfirst=0)
WHEN(Clen == 20)
AT(0, 0, 22.85) RELATIVE source

COMPONENT chopper20_3 = DiskChopper(radius=0.3,yheight=0.07, theta_0=83.685/ChopperSlit, nu=-14/(2*ChopperSlit), nslit=2*ChopperSlit, delay=D3/ChopperSlit,isfirst=0)
WHEN(Clen == 20)
AT(0, 0, 26.3) RELATIVE source

COMPONENT chopper20_4 = DiskChopper(radius=0.3,yheight=0.07, theta_0=84.635/ChopperSlit, nu=14/(2*ChopperSlit), nslit=2*ChopperSlit, delay=D4/ChopperSlit,isfirst=0)
WHEN(Clen == 20)
AT(0, 0, 26.6) RELATIVE source

COMPONENT flightGravity_20 = Guide_gravity(w1=2.0, h1=2.0, w2=2.0, h2=2.0, l=9.39, R0=0.99, Qc=0.02174, alpha=6.07, m=0, W=0.003)
WHEN(Clen == 20.0)
AT(0, 0, 26.601) RELATIVE source
//currentPos(26.31)+=0.28=26.59

/*Test different delay times due to wrong transmitted wavelength band*/
/*delay time =  half opened time + opening time */

/**********************************************
14 m Collimation setting

Clen == 14

Position of Aperture at 16+6 = 22m
**********************************************/

COMPONENT guide14col_1 = Guide_gravity(w1=NGblen, h1=NGblen, w2=NGblen, h2=NGblen, l=4.49, R0=0.99, Qc=0.02174, alpha=6.07, m=dfltM, W=0.003)
WHEN((Clen == 14))
AT(0, 0, 11.0) RELATIVE source
//currentPos(11.0)+=4.49=15.49

COMPONENT chopper14_1 = DiskChopper(radius=0.3,yheight=0.07, theta_0=98.64/ChopperSlit, nu=14/ChopperSlit, nslit=ChopperSlit, delay=D1/ChopperSlit ,isfirst=0)
WHEN(Clen == 14)
AT(0,0,/*currentPos=*/15.5) RELATIVE source
//currentPos=15.5

COMPONENT guide14col_2 = Guide_gravity(w1=NGblen, h1=NGblen, w2=NGblen, h2=NGblen, l=6.48, R0=0.99, Qc=0.02174, alpha=6.07, m=dfltM, W=0.003)
WHEN((Clen == 14))
AT(0, 0, 15.51) RELATIVE source
//currentPos(15.51)+=6.48=21.99

/**********************************************
Collimation aperture for 14 m collimation. Has
to be at 22 m
**********************************************/
COMPONENT colap_14 = Slit(xmin= -colWidth/2, xmax=colWidth/2, ymin = -colWidth/2, ymax=colWidth/2)
WHEN(Clen == 14)
AT(0,0,22.0) RELATIVE source
//currentPos=22.0

COMPONENT chopper14_2 = DiskChopper(radius=0.3,yheight=0.07, theta_0=145.41/ChopperSlit, nu=14/ChopperSlit, nslit=ChopperSlit, delay=D2/ChopperSlit,isfirst=0)
WHEN(Clen ==14)
AT(0, 0, 22.85) RELATIVE source
//currentPos=22.85

COMPONENT chopper14_3 = DiskChopper(radius=0.3,yheight=0.07, theta_0=83.685/ChopperSlit, nu=14/(2*ChopperSlit), nslit=2*ChopperSlit, delay=D3/ChopperSlit,isfirst=0)
WHEN(Clen == 14)
AT(0, 0, 26.3) RELATIVE source

COMPONENT chopper14_4 = DiskChopper(radius=0.3,yheight=0.07, theta_0=84.635/ChopperSlit, nu=14/(2*ChopperSlit), nslit=2*ChopperSlit, delay=D4/ChopperSlit,isfirst=0)
WHEN(Clen == 14)
AT(0, 0, 26.6) RELATIVE source

COMPONENT flightGravity_14 = Guide_gravity(w1=2.0, h1=2.0, w2=2.0, h2=2.0, l=9.39, R0=0.99, Qc=0.02174, alpha=6.07, m=0, W=0.003)
WHEN(Clen == 14.0)
AT(0, 0, 26.601) RELATIVE source
//currentPos(26.31)+=0.28=26.59

/**********************************************
8 m Collimation setting

Clen == 8
**********************************************/

COMPONENT guide8col_1 = Guide_gravity(w1=NGblen, h1=NGblen, w2=NGblen, h2=NGblen, l=4.49, R0=0.99, Qc=0.02174, alpha=6.07, m=dfltM, W=0.003)
WHEN((Clen == 8))
AT(0, 0, 11.0) RELATIVE source
//currentPos(11.0)+=4.49=15.49

COMPONENT chopper8_1 = DiskChopper(radius=0.3,yheight=0.07, theta_0=98.64/ChopperSlit, nu=14/ChopperSlit, nslit=ChopperSlit, delay=D1/ChopperSlit ,isfirst=0)
WHEN(Clen == 8)
AT(0,0,/*currentPos=*/15.5) RELATIVE source
//currentPos=15.5

//COMPONENT chopper8_1 = DiskChopper(radius=0.3,yheight=0.07, theta_0=98.64/ChopperSlit, nu=14/ChopperSlit, nslit=ChopperSlit, delay=D1/ChopperSlit ,isfirst=0)
//WHEN(Clen == 8)
//AT(0,0,15.5) RELATIVE source
//currentPos=15.5

COMPONENT guide8col_2 = Guide_gravity(w1=NGblen, h1=NGblen, w2=NGblen, h2=NGblen, l=7.33, R0=0.99, Qc=0.02174, alpha=6.07, m=dfltM, W=0.003)
WHEN((Clen == 8))
AT(0, 0, 15.51) RELATIVE source
//currentPos(15.51)+=7.33=22.84

COMPONENT chopper8_2 = DiskChopper(radius=0.3,yheight=0.07, theta_0=145.41/ChopperSlit, nu=14/ChopperSlit, nslit=ChopperSlit, delay=D2/ChopperSlit,isfirst=0)
WHEN(Clen ==8)
AT(0, 0, 22.85) RELATIVE source
//currentPos=22.85

COMPONENT guide8col_3 = Guide_gravity(w1=NGblen, h1=NGblen, w2=NGblen, h2=NGblen, l=3.43, R0=0.99, Qc=0.02174, alpha=6.07, m=dfltM, W=0.003)
WHEN((Clen == 8))
AT(0, 0, 22.86) RELATIVE source
//currentPos(22.86)+=3.43=26.29

COMPONENT chopper8_3 = DiskChopper(radius=0.3,yheight=0.07, theta_0=83.685/ChopperSlit, nu=14/(2*ChopperSlit), nslit=2*ChopperSlit, delay=D3/ChopperSlit,isfirst=0)
WHEN(Clen == 8)
AT(0, 0, 26.3) RELATIVE source

COMPONENT guide8col_4 = Guide_gravity(w1=NGblen, h1=NGblen, w2=NGblen, h2=NGblen, l=0.28, R0=0.99, Qc=0.02174, alpha=6.07, m=dfltM, W=0.003)
WHEN((Clen == 8))
AT(0, 0, 26.31) RELATIVE source
//currentPos(26.31)+=0.28=26.59

COMPONENT chopper8_4 = DiskChopper(radius=0.3,yheight=0.07, theta_0=84.635/ChopperSlit, nu=-14/(2*ChopperSlit), nslit=2*ChopperSlit, delay=D4/ChopperSlit,isfirst=0)
WHEN(Clen == 8)
AT(0, 0, 26.6) RELATIVE source

COMPONENT guide8col_5 = Guide_gravity(w1=NGblen, h1=NGblen, w2=NGblen, h2=NGblen, l=1.38, R0=0.99, Qc=0.02174, alpha=6.07, m=dfltM, W=0.003)
WHEN((Clen == 8))
AT(0, 0, 26.61) RELATIVE source
//currentPos(26.61)+=1.38=27.99

/***************************
should be at 28 m to accomodate 8m wide collimation housing  --> sample pos at 36.1m
********************************************/
COMPONENT colap_8 = Slit(xmin= -colWidth/2, xmax=colWidth/2, ymin = -colWidth/2, ymax=colWidth/2)
WHEN(Clen ==8.0)
AT(0, 0, 28.0) RELATIVE source
//currentPos=28.0

COMPONENT flightGravity = Guide_gravity(w1=2.0, h1=2.0, w2=2.0, h2=2.0, l=7.9, R0=0.99, Qc=0.02174, alpha=6.07, m=0, W=0.003)
WHEN(Clen == 8.0)
AT(0, 0, 28.01) RELATIVE source
//currentPos(28.002)+7.9=35.902


/**********************************************
Monitor Component at the Exit of the Chopper
section
**********************************************/
//COMPONENT ChopperExitMonitor=Monitor_Radius  (xmin = -.5*NGblen, xmax = 0.5*NGblen, ymin = -.5*NGblen, ymax = 0.5*NGblen,options  = "hdiv bins=100 limits[-2.5 2.5]; lambda bins=50 limits[1.0 20.0]", 
//filename = "MonExitChopper.psd", num=ChopperSlit)
//AT(0,0,/*currentPos=*/28.0002) RELATIVE source

/**********************************************
Sample Position
**********************************************/

COMPONENT smpap  = Slit(xmin=-0.5*SampD,xmax=0.5*SampD,ymin=-0.5*SampD,ymax=0.5*SampD)
  AT (0, 0,/*currentPos=*/36.0) RELATIVE source
//currentPos=36.0

COMPONENT vout = MCPL_output(filename="skadiSampleAperture.mcpl", verbose=1,userflag=flag,userflagcomment="")
  AT(0,0,36.01) RELATIVE source



FINALLY
%{
%}
END
