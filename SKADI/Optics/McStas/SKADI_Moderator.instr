/*******************************************************************************
*
* McStas, neutron ray-tracing package
*         Copyright 1997-2002, All rights reserved
*         Risoe National Laboratory, Roskilde, Denmark
*         Institut Laue Langevin, Grenoble, France
*
* Instrument: SKADI Standard design after final bunker design
*
* Sebastian Jaksch
*
*
* INPUT PARAMETERS:
* 
*******************************************************************************/
DEFINE INSTRUMENT SKADI (NGblen=0.03,                  /*width of the neutron guide blinds*/
                                /*ScPlus=0.07,*/
                                Clen = 20.0,                 /*length of the collimation*/
                                SampD=0.01,                 /*Diameter of sample*/
                                bendL1=4.49,                 /*length of upstream bender part of the initial S-bender*/
                                bendL2=4.49,                 /*length of downstream bender part of the initial S-bender*/
                                bendR1 = 84.0,             /*bending radius of upstream bender part of the initial S-bender*/
                                bendR2=84.0,                 /*bending radius of downstream bender part of the initial S-bender*/
                                bendM = 3.0,                /*m-value at the concave side of the S-bender*/
                                bendiM = 3.0,               /*m-value at the convex side of the S-bender*/
                                dfltM = 3.0,                /*default m-value throughout the instrument*/
                                //MDOWN = 3.6,                /*m-value of the bottom side of the polarizing supermirror*/
                                //Min=1.5,                    /*m-value of the neutron guide surrounding the polarizing supermirror*/
                                //cnum=0.00,                  /*switch for the polarizer (switched on for cnum>0.5)*/
                                //ROT=0.0,                    /*rotation of the instrument*/
                                modnum= 11.0,               /*select a sample type, see description of SANS_benchmark2 component*/
                                incs=0.0005,                /*the incoherent background from the overall sample (cm-1), should read ca. 1.0 for water, 0.5 for half D2O, half H2O, and ca. 0.02 for D2O*/
                                npulses = 4.0,              /*number of pulses to be simulated*/
                                ChopperSlit=1,               /*Slits in chopper disks*/
                                colWidth=0.02,        /*width of the collimations slits in x and y direction*/
        D1=0.02152,      /*Delay times for choppers 1-4*/  
        D2=0.03174,
        D3=0.03653,
        D4=0.03845
        )

/* keep default values as much as possible */

DECLARE
%{
  #ifndef M_PI
  #define M_PI 3.14159265358979323846
  #endif
  int32_t flag;
  double lambdamin,lambdamax;
  char SANSareaName[256];
  char MonShortName[256];
  char MonLongName[256];
  char numchar[10];
  double currentPos=0;
%}
INITIALIZE
%{
  lambdamin=0.2;
  lambdamax=70;
  sprintf(SANSareaName,"SANSarea_det_Chopper_");
  sprintf(MonShortName,"MonitorShortDistance_Chopper_");
  sprintf(MonLongName,"MonitorLongDistance_Chopper_");
  //sprintf(numchar,"%g",ChopperSwitch);
  strncat(SANSareaName,numchar,256);
  strncat(MonShortName,numchar,256);
  strncat(MonLongName,numchar,256);
  /*number of slits*/
  strncat(MonShortName,".txt",256);
  strncat(MonLongName,".txt",256);
%}
TRACE

COMPONENT origin = Arm()
  AT (0,0,0) ABSOLUTE

/**********************************************
currentPos variable to define the current position
for absolute distances with respect to the source
**********************************************/

//ESS Source with physically feasible parameters for opening etc.
COMPONENT source=ESS_moderator(sourcedef="2014", Lmin=lambdamin, Lmax=lambdamax, dist=2.0,focus_xw=NGblen, focus_yh=NGblen, xwidth_c=0.23, xwidth_t=0.12,extraction_opening=120)
  AT (0, 0, 0) RELATIVE origin

COMPONENT rspin  = Spin_random ()
  AT (0, 0, 0.0001) RELATIVE source

COMPONENT voutBender = MCPL_output(filename="skadiModerator.mcpl", verbose=1,userflag=flag,userflagcomment="")
AT(0,0,0.03) RELATIVE source



FINALLY
%{
%}
END
