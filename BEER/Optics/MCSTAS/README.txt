McStas model of the diffractometer BEER@ESS
February 2017
============================================
Contents:
Instrument file: BEER_primary.instr
Dpendences: Guide_multichannel.comp
Example scripts: run.sh, run.par (shell script and input parameters)
--------------------------------------------
The model simulates the diffractometer BEER from the moderator (ESS_butterfly.comp) to the sample position. It includes the detailed list of components as specified in the TG2 documents. The model depends on the contributed component Guide_multichannel.comp, which describes the bi-spectral switch component in the monolith, solving neutron transport through the array of semi-transparent Si wafers coated with a supermirror. 
See documentation in the source files for more details. 
The instrument model has been cross-checked by simulations of an equivalent instrument configuration in SIMRES. 
-------------------------------------------------------
Usage:
-------
The interface permits to set the resolution and wavelength selection modes, wavelength range, sample slit, move the focusing guide on/off, and put the bi-spectral switch blades on/off. 

The pre-selected operation modes are defined below. They affect how the chopper speeds, phases, focusing guide and divergence slits are configured. The chopper phases are automatically set for the nominal wavelength (lambda). The constant t_offset defined in the code defines time zero with respect to the proton pulse. Other modes can be set manually by editing the code in the INITIALIZE section. 
Optionally, neutrons at the sample slit can be saved in an MCPL file. 

Resolution modes (mode):
----------------------------------
0 .. full beam, choppers stopped
1 .. high resolution, PSC1+PSC2
2 .. medium resolution, PSC1+PSC2
3 .. low resolution, PSC1+PSC3
4 .. modulation, high resolution, MCA, 280 Hz
5 .. modulation, medium resolution, MCA, 70 Hz
6 .. modulation, medium resolution, MCB, 280 Hz
7 .. imaging, L/D ~ 300, PSC1 + PSC3
Note that modes 3 and 6 are not feasible within the limited BEER scope (missing choppers PSC3, MCB, MCC).

Wavelength selection modes (wmode):
---------------------------------------
0 .. no selection
1 .. single frame
2 .. pulse suppression (double frame) 
 
 

