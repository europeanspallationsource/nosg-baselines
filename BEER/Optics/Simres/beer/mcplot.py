# -*- coding: utf-8 -*-
"""
Plots results generated by McStas simulation.

Created on Sat Apr 27 13:23:58 2019
@author: Jan Saroun, saroun@ujf.cas.cz
Copyright (c) 2020 Nuclear Physics Institute, CAS, Rez
"""


import numpy as np
from matplotlib import pyplot as plt
import os
import re

#%% Basic data structures

pnames = ['values', 'xlabel', 'ylabel', 'xlimits', 'title']



class Data1D:
    """
    Data structure for 1D plot
    """
    def __init__(self, params, dformat, data=None):
        self.title=''
        self.xlabel=''
        self.ylabel=''
        self.xlimits=[0,1]
        self.data=None
        self.legend=''
        self.fname=''
        self.comment=''
        self.dformat=dformat
        self.vals=[0, 0, 0]
        for key in params.keys():
            if (key=='xlabel'):
                self.xlabel=params['xlabel']
            elif (key=='ylabel'):
                self.ylabel=params['ylabel']  
            elif (key=='title'):
                self.title=params['title']
            elif (key=='comment'):
                self.comment=params['comment']
            elif (key=='xlimits'):
                self.xlimits=params['xlimits']
            elif (key=='values'):
                self.vals = params['values'] 
        if (data is not None):
            self.data=data
    
    def summary(self):
        out = 'Data file:\t{}\n'.format(self.fname)
        out += 'Title:\t{}\n'.format(self.title)
        if self.data[0]:
            out += 'Rows:\t{:d}\n'.format(len(self.data[0]))
        out += 'Intensity:\t{:g} +- {:g}\n'.format(self.vals['sum'], self.vals['e_sum'])
        return out
    
    def calValues(self):
        """
        Calculate integral parameters: intensity, spread, mean.
        Return as dict.
        """
        r8ln2 = np.sqrt(8*np.log(2))
        if not self.data:
            return
        nc = len(self.data)
        x = np.array(self.data[0])
        y = np.array(self.data[1])
        if nc>2:
            err = np.array(self.data[2])
        else:
            err = None
        sum0 = np.sum(y)
        sum1 = y.dot(x)
        sum2 = y.dot(x**2)
        integ = sum0
        if sum0>0:
            mean = sum1/sum0
            stdev = np.sqrt(sum2/sum0 - mean**2)
        else:
            mean = 0.0
            stdev = 0.0
        fwhm = r8ln2*stdev
        res = {'sum': integ,
               'width': fwhm,
               'mean': mean}
        if (sum0>0) and (err is not None):
            res['e_sum'] = np.sqrt(np.sum(err**2))
            r = (x - mean)*err/sum0
            res['e_mean'] = np.sqrt(r.dot(r))
            r = (x-mean)**2 - stdev**2
            r = r*err/sum0
            res['e_width'] = r8ln2*np.sqrt(r.dot(r))/stdev
        return res


#%% Parsers of simulation results

"""
 Define a key for each item from pnames. The key is used to search for 
given parameter in file headers. 
"""
# For McSTas
keysmc = {'values':'values',
          'xlabel':'xlabel',
          'ylabel':'ylabel',
          'xlimits':'xlimits',
          'title':'title'}

# For SIMRES:
# NOTE: intensity is calculated from the data, hence values=None.
keyssim = {'X-TITLE':'xlabel',
           'Y-TITLE':'ylabel',
           'X-RANGE':'xlimits',
           'TITLE':'title',
           'COMMENT':'comment'
          }

       
def parseHdrMcStas(line):
    """
    Extracts parameters from a single text line of a McStas format header. 
    
    Parameter keys are defined as a list `keysmc`.
    
    Returns:
    --------
    Hash map with found parameter keys and values.
    """
    res = {}
    if not line.startswith('#'):
        return res                  
    s =line[1:].replace(';',' ').strip()
    # get name, value pair (separated by the 1st :)
    inp = s.split(':')
    if (len(inp)<2):
        return res
    name = inp[0].strip()
    # skip if name is not in keys
    if not name in keysmc:
        return res
    # get a variable key, we already know it exists
    key = keysmc[name]
    # join all other parts separated by : if any
    if len(inp)>2:
        val = ':'.join(inp[1:])
    else:
        val = inp[1]
    # extract all strings in quotes into a list of strings
    if val.find('"')>=0:
        vals = re.findall(r'"(.*?)"', val)
    else:
        vals = [val.strip()]
    
    # now we need to to convert specific variables into valid data:
    # expect a pair of floats
    if name=='xlimits':
        s = vals[0].split()
        if len(s)>1:
            res[key] = [float(s[0]),float(s[1])]
    # expect 3 floats
    if name=='values':
        s = vals[0].split()
        if len(s)>1:
            v = {}
            v['sum'] = float(s[0])
            v['e_sum'] = float(s[1])
            res[key] = v
    # expect a single string
    else:
        res[key] = ''.join(vals)              
    return res

           
def parseHdrSIMRES(ln):
    """
    Extracts parameters from a single text line of a SIMRES format header.
    
    Parameter keys are defined as a list `keyssim`.
    
    Returns:
    --------
    Hash map with found parameter keys and values.
    """
    res = {}
    if ln.find(':')<0:
        return res
    
    # get name, value pair (separated by the 1st :)
    inp = ln.strip().split(':')
    name = inp[0].strip()
    # skip if name is not in keys
    if not name in keyssim:
        return res
    # get a variable key, we already know it exists
    key = keyssim[name]
    # join all other parts separated by : if any
    if len(inp)>2:
        val = ':'.join(inp[1:])
    else:
        val = inp[1]
    # extract all strings in quotes into a list of strings
    if val.find('"')>=0:
        vals = re.findall(r'"(.*?)"', val)
    else:
        vals = [val.strip()]
    # now we need to to convert specific variables into valid data:
    # expect a pair of floats
    if name=='X-RANGE':
        s = vals[0].split()
        if len(s)>1:
            res[key] = [float(s[0]),float(s[1])]
    # expect a single string
    else:
        res[key] = ''.join(vals)
    return res
    

def parseFileMcStas(fname, yscale=1):
    """
    Loads a data file and converts its content to a Data1D object. 
    
    The input file must be a McStas 1-dim monitor output file.
    """
    params = {}
    x = []
    y = []
    err = []
    with open(fname, 'r') as f:
        lines = f.readlines()
        for L in lines:
            if (L.startswith('#')):
                p = parseHdrMcStas(L)
                if (len(p)>0):
                    params = {**params, **p}
            else:
                s = L.split()
                if (len(s)>2):
                    x += [float(s[0])]
                    y += [float(s[1])*yscale]
                    err += [float(s[2])*yscale]
        f.close()
    data = [x, y, err]
    out = Data1D(params, 'mcstas', data=data)
    out.vals = out.calValues()
    out.fname = fname
    return out


def parseFileSIMRES(fname, yscale=1):
    """
    Loads a data file and converts its content to a Data1D object. 
    
    The input file must be a SIMRES BEAM1D graph data file.
    """
    params = {}
    x = []
    y = []
    err = []
    sect = 0
    # assume 2 columns by default
    nc = 2
    with open(fname, 'r') as f:
        lines = f.readlines()
        for L in lines:
            # start of the data section
            if L.strip().startswith('DATA_1D'):
                sect = 1
            # read header
            elif sect==0:
                p = parseHdrSIMRES(L)
                if (len(p)>0):
                    params = {**params, **p}
            # data section has 1 header line. 
            # get number of columns from here
            elif sect==1:
                s = L.strip().split()
                nc = len(s)
                sect = 2
            elif sect==2:
                s = L.split()
                if not len(s)==nc:
                    msg = 'Invalid data format, number of columns does not match header.'
                    raise Exception(msg)
                if nc<2:
                    msg = 'Invalid data format, number of columns < 2'
                    raise Exception(msg)
                if nc>=2:
                    x += [float(s[0])]
                    y += [float(s[1])*yscale]
                if nc>=3:
                    err += [float(s[2])*yscale]
        f.close()
    if nc==3:
        data = [x, y, err]
    else:
        data = [x, y]
    out = Data1D(params, 'simres', data=data)
    out.vals = out.calValues()
    out.fname = fname
    return out

#%% Functions operating on Data1D objects

def plot1D(ax, dset, xscale=1, yscale=1, showvalue=True):
    """
    Plot given data (class Data1D) on provided axis. 
    Optionally, scale the x,y axes by provided factors (xscale, yscale).
    
    Arguments:
    ---------
    ax: matplotlib.pyplot.Axes
        axis object to plot on
    dset: beer.mcplot.Data1D
        data set to be plotted
    xscale: float
        x-axis scaling
    yscale: float
        y-axis scaling
    """
    MEDIUM_SIZE = 10
    BIGGER_SIZE = 12 
    
    # table for special character replacement, PGPLOT -> LaTeX
    reptable = {'@@d(.*)@@u':'_{\\1}',
                '@@u':'^',
                '@@gl':'{@@lambda}',
                '@@gD':'{@@Delta}',
                '@@A':'{@@AA}',
                '@@m':'{@@mu}'
                }
                
    def replChar(s):
        """ Convert special characters from PGPLOT to LaTeX math format """
        if s.find('\\')>=0:
            # avoid problems with escape ...
            ss = re.sub(r'\\','@@',s)
            for c in reptable:
                ss = re.sub(c,reptable[c],ss)
                #if ss.find(c)>=0:
                    #ss = re.sub(c,reptable[c],ss)
            ss = re.sub('@@',r'\\',ss)
            out = '${}$'.format(ss)
        else:
            out = s
        return out
    
    def getx(d):
        """ 
        Consolidate x-label text
        """
        xsc = xscale
        xlbl = d.xlabel
        q = xlbl.find('[\gms]')
        # convert microsec. to ms
        if q>0:
            xlbl = xlbl.replace('[\gms]','[ms]')
            xsc=0.001
        # convert other special characters to LaTeX math format
        xlbl = replChar(xlbl)
        # This works only for BEER_reference.instr with pre-defined output file names:
        # Is there another way how to find out that the horizontal monitor 
        # is actually used as vertical?
        q = d.fname.find('YMon.dat')
        if q>0:
            xlbl = xlbl.replace('x-Position','y-Position')    
        q = d.fname.find('VDivMon.dat')
        if q>0:
            xlbl = xlbl.replace('horizontal','vertical')              
        return [xlbl, xsc]
    
    if len(dset)<=0:
        return
    [xlbl, xsc] = getx(dset[0])
    ymax = 0
    for dd in dset:
        ymax =  max(ymax,np.max(dd.data[1])*yscale)
    ax.set_ylim(0, ymax*1.05) 
    ax.set_xlabel(xlbl, fontsize=MEDIUM_SIZE)
    ax.set_ylabel(dset[0].ylabel, fontsize=MEDIUM_SIZE)
    ax.minorticks_on()
    ax.tick_params(axis='both', labelsize=MEDIUM_SIZE)
    title = dset[0].title
    # add intensity info if required 
    if showvalue and (len(dset)==1) and dset[0].vals:
        sval = 'I = {:.3g}'.format(dset[0].vals['sum'])
        title += ' ; '+sval
        #ax.text(0.95, 0.95, sval, horizontalalignment='right',
        #        verticalalignment='top', transform=ax.transAxes)
    ax.set_title(title, fontsize=BIGGER_SIZE)
    for dd in dset:
        ax.errorbar(np.multiply(dd.data[0],xsc), np.multiply(dd.data[1],yscale), label=dd.legend)
    if dd.legend:
        ax.legend()

#%% Top level functions for simulation results processing

def processResults(results, dataname='Lmon.dat', parentdir='./', outfile='', 
                dformat='mcstas', yscale=1):
    """
    Process a set of simulation results from McStas or SIMRES. 
    For each result, find given data file (dataname) and convert it 
    to the Data1D object. Print and (optionally) save the integral intensities 
    for all results as a table.    

    **For McStas**
    
    Each simulation result is found in a separate directory, `results` is 
    a list of these directories (relative to `parentdir'.
    
    `Example: results=['A', 'B', 'C'], dataname='xmon.dat'` 
    
    would process all files 'xmon.dat' found in the subdirectories 
    'A', 'B' and 'C' relative to parentdir. 
    
    **For SIMRES**
    
    All simulations are saved in a single output directory. We assume that
    the file names are constructed as `[basename][suffix]`, where `basename` denotes
    the simulation and `suffix` denotes the type of output (plot). Then you 
    can provide a list of basenames as `results`, whereas `dataname` should be 
    the required suffix.
    
    `Example: results=['A', 'B', 'C'], dataname='_xmon.dat'` 
    
    would process files ``A_xmon.dat``, ``B_xmon.dat`` and ``C_xmon.dat`` from the 
    directory ``parentdir``.
    
    Parameters:
    -----------
    results: list of str
        list of directories (McStas) or file name bases (SIMRES) to search for
    dataname: str
        file name (McStas) or file name suffix (SIMRES) to search for
    parentdir: str
        parent directory for search
    outfile: str
        output file name
    dformat: str
        'mcstas' or 'simres', defines data format
    yscale: float
        scale factor to apply for y-values 
    """
    
    out = "# ID\tintensity\terr\n"
    fmt = "{}\t{:.3f}\t{:.3f}\n"
    datas = []
    for fn in results:
        p = None
        try:
            if dformat=='mcstas':
                f = os.path.join(parentdir,fn, dataname)
                f = os.path.normpath(f)
                p = parseFileMcStas(f, yscale=yscale)
            elif dformat=='simres':
                f = os.path.join(parentdir,fn+dataname)
                f = os.path.normpath(f)
                p = parseFileSIMRES(f, yscale=yscale)
        except Exception as e:
            print(e)
        if p:
            if 'e_sum' in p.vals:
                out += fmt.format(fn,p.vals['sum'],p.vals['e_sum'])
            else:
                out += fmt.format(fn,p.vals['sum'],0.0)
            datas.append(p)
    print(out)
    if outfile:
        fout = os.path.normpath(os.path.join(parentdir,outfile))
        with open(fout, 'w') as fo:
            fo.write(out)
            fo.close()
        print('Table of intensities saved in "{}"\n'.format(fout))
    return datas


def processRun(dirname, files=['Lmon.dat', 'TofMon.dat'], parentdir='./', 
               dformat='mcstas', yscale=1):
    """
    Process a single simulation result from McStas or SIMRES. 
    Find all specified files in given directory and convert them 
    to the `Data1D` objects. Return these objects as a list.
    
    Parameters:
    -----------
    dirname: str
        directory to search for files in (relative to parentdir)
    files: list of str
        file names to search for (should be output data from 1-dim plots 
        in McStas of SIMRES)
    parentdir: str
        parent directpry for search
    dformat: str
        'mcstas' or 'simres', defines data format
    yscale: float
        scale factor to apply for y-values
        
    """
    datas = []
    nf = len(files)
    for i in range(nf):
        if (i<nf):
            f = files[i]
            file_path = os.path.join(parentdir, dirname, f)
            file_path = os.path.normpath(file_path)
            if dformat=='simres':
                d =  parseFileSIMRES(file_path, yscale=yscale)
            # assume McStas by default
            else:
                d =  parseFileMcStas(file_path, yscale=yscale)
            datas.append(d)
    return datas


def plotDataSet(datas, yscale=1, title='', pdf=''):
    """
    Plot a set of data given as a list of Data1D objects
    
    Arguments:
    ---------
    datas: list of Data1D
        list of data objects for 1-dim monitors
    yscale: float
        scale plot y-axis by given factor
    title: str
        a common title for the plot
    pdf: str
        filename for output figure in PDF format 
        (leave empty to suppress this feature)
    
    """
    if len(datas)<=0:
        print('Nothing to plot')
        return
    xwidth = 6
    yheight = 4
    nf = len(datas)
    nc = min(2,nf)
    nr = int(nf/2-0.001)+1
    fig, axs = plt.subplots(nrows=nr, ncols=nc, figsize=(nc*xwidth,nr*yheight))
    # convert single axis to a list
    nax = np.size(axs)
    if nax==1:
        axx = [axs]
    else:
        axx = axs.reshape((nax,))
    for i in range(nax):
        if (i<nf):
            d = datas[i]
            fn = os.path.basename(d.fname)
            dn = os.path.basename(os.path.dirname(d.fname))
            d.title = 'file = ' + os.path.normpath(os.path.join(dn,fn))
            plot1D(axx[i], [d])
        else:
            axx[i].axis('off')
    if title:
        fig.suptitle(title, fontsize=14)
    fig.tight_layout(rect=[0, 0.03, 1, 0.95])
    if pdf:
        pname = os.path.normpath(pdf)
        if not pname.endswith('.pdf'):
            pname += '.pdf'
        plt.savefig(pname, bbox_inches='tight')
        print('Plot saved as "{}"'.format(pname))
    plt.show()



