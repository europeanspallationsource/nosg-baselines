#!/usr/bin/env python
#-*- coding: utf-8 -*-
import sys
from numpy import *
seterr(all='ignore')
import estia_help as eh
import mcstas_reader as mr


#ref=mr.McSim('../results/pol_ref_10x10_10/')['tof_detector']
#pol1=mr.McSim('../results/pol1_10x10_10/')['tof_detector']
#pol2=mr.McSim('../results/pol2_10x10_10/')['tof_detector']

p0=linspace(-1,1,2001)
p=(p0[1:]+p0[:-1])/2.
l0=linspace(1,30,291)
l=(l0[1:]+l0[:-1])/2.

def get_trans(ds):
    x,I=ds.project1d('L', bins=l0, fltr='(x>-0.06)&(y<0.15)')
    x,Ir=ref.project1d('L', bins=l0, fltr='(x>-0.06)&(y<0.15)')
    return I/Ir

def get_ref(ds):
    x,I=ds.project1d('L', bins=l0, fltr='(x>-0.06)&(y>0.15)')
    x,Ir=ref.project1d('L', bins=l0, fltr='(x>-0.06)&(y<0.15)')
    return I/Ir

def get_pol(ds):
    x,y,I=ds.project2d('L', 'sy', bins=(p0,l0), fltr='(x>-0.06)&(y<0.15)')
    return (I*p[:,newaxis]).sum(axis=0)/I.sum(axis=0)

def get_ana(ds):
    x,y,It=ds.project2d('L', 'sx', bins=(p0,l0), fltr='(x>-0.06)&(y<0.15)')
    x,y,Ir=ds.project2d('L', 'sx', bins=(p0,l0), fltr='(x>-0.06)&(y>0.15)')
    return (It*p[:,newaxis]).sum(axis=0)/It.sum(axis=0),(Ir*p[:,newaxis]).sum(axis=0)/Ir.sum(axis=0),  
    


if __name__=='__main__':
    pass
