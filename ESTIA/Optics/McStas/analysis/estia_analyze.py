#!/usr/bin/env python
#-*- coding: utf-8 -*-
'''
Main script that runs the full analysis for the estia baseline McStas simulation.
'''

import sys, os
from numpy import *
seterr(all='ignore')
import estia_help as eh
import mcstas_reader as mr


# Scaling factor of source monitor after normalizing by area to get to brilliance.
# Converts from 1.5x4.0 degree² acceptance range to 1/steradian
# Factor 10 due to 0.1 Angstrom binning.
BT_SOURCE_SCALE=10.*(180.**2/pi**2)/1.5/4.0
BT_SAMPLE_SCALE=10.*(180.**2/pi**2)/1.5/1.5


if __name__=='__main__':
  if not os.path.exists('../results/analyzed'):
    os.mkdir('../results/analyzed')
  
  if len(sys.argv)>1:
    only_items=map(int, sys.argv[1:])
  else:
    only_items=range(100)

  if 1 in only_items:
    print "Brilliance Transfer 5x10"
    bt_10=mr.McSim('../results/brilliance_5x10/mccode.h5')
    d=bt_10['tof_sample.L']
    vs=bt_10['tof_virtual_source.L']
    r=bt_10['tof_source.L']
    x, y, e=eh.calc_brilliance_transfer(d, BT_SAMPLE_SCALE*2., r, BT_SOURCE_SCALE)
    savetxt('../results/analyzed/brilliance_transfer_5x10.dat', array([x, y, e]).T)
    savetxt('../results/analyzed/brilliance_5x10.dat', array([x, d.data*BT_SAMPLE_SCALE*2.,
                                           d.errors*BT_SAMPLE_SCALE*2.]).T)
    x, y, e=eh.calc_brilliance_transfer(vs, BT_SAMPLE_SCALE, r, BT_SOURCE_SCALE)
    savetxt('../results/analyzed/brilliance_transfer_vs_5x10.dat', array([x, y, e]).T)
    
    btn_10=mr.McSim('../results/brilliance_nowindow_5x10/mccode.h5')
    d=btn_10['tof_sample.L']
    vs=btn_10['tof_virtual_source.L']
    r=btn_10['tof_source.L']
    x, y, e=eh.calc_brilliance_transfer(d, BT_SAMPLE_SCALE*2., r, BT_SOURCE_SCALE)
    savetxt('../results/analyzed/brilliance_transfer_nowindow_5x10.dat', array([x, y, e]).T)
    x, y, e=eh.calc_brilliance_transfer(vs, BT_SAMPLE_SCALE, r, BT_SOURCE_SCALE)
    savetxt('../results/analyzed/brilliance_transfer_nowindow_vs_5x10.dat', array([x, y, e]).T)


  if 2 in only_items:
    print "Brilliance Transfer 1x1"
    bt_1=mr.McSim('../results/brilliance_1x1/mccode.h5')
    d=bt_1['tof_sample.L']
    r=bt_1['tof_source.L']
    x, y, e=eh.calc_brilliance_transfer(d, BT_SAMPLE_SCALE*100, r, BT_SOURCE_SCALE)
    savetxt('../results/analyzed/brilliance_transfer_1x1.dat', array([x, y, e]).T)
    savetxt('../results/analyzed/brilliance_1x1.dat', array([x, d.data*BT_SAMPLE_SCALE*100,
                                           d.errors*BT_SAMPLE_SCALE*100]).T)

  if 3 in only_items:
    print "Refelctiviy 10x10 sample"
    opts=dict(qres=0.01, qmin=0.003, qmax=0.5, mindq=5e-4)
    names=['q_z', 'Intensity(ToF)', 'Reflectivity(ToF)', 'dR (1s ToF)', 'Intensity(λ)', 'Reflectivity(λ)']
    units=['A^{-1}', 'cts/s', '1', 'cts/s', '1']
    info='Reflectivity simulation for 10x10mm² sample, omega=%.1f deg'

    print "0.8 degree"
    ref=mr.McSim('../results/reference_10x10_10/mccode.h5')['tof_detector']
    ni_08=mr.McSim('../results/nickle_10x10_08/mccode.h5')['tof_detector']
    q, I, R=eh.calcR(ni_08, ref, 0.8, 1.0, detcorr=True, **opts)
    q, I_real, R_real=eh.calcR(ni_08, ref, 0.8, 1.0, use_tof=False, **opts)
    idx_start, idx_end=where((I>0.)&(I_real>0.))[0][[0,-1]]
    eh.save_w_header('../results/analyzed/reflectivity_10x10_08.dat',
                     [q[idx_start:idx_end+1],
                      I[idx_start:idx_end+1], R[idx_start:idx_end+1],
                      (R/sqrt(I))[idx_start:idx_end+1],
                      I_real[idx_start:idx_end+1], R_real[idx_start:idx_end+1]],
                     info, names, units)
    del(ni_08)

    print "3.0 degree"
    ni_30=mr.McSim('../results/nickle_10x10_30/mccode.h5')['tof_detector']
    q, I, R=eh.calcR(ni_30, ref, 3.0, 1.0, detcorr=True, **opts)
    q, I_real, R_real=eh.calcR(ni_30, ref, 3.0, 1.0, use_tof=False, **opts)
    idx_start, idx_end=where((I>0.)&(I_real>0.))[0][[0,-1]]
    eh.save_w_header('../results/analyzed/reflectivity_10x10_30.dat',
                     [q[idx_start:idx_end+1],
                      I[idx_start:idx_end+1], R[idx_start:idx_end+1],
                      (R/sqrt(I))[idx_start:idx_end+1],
                      I_real[idx_start:idx_end+1], R_real[idx_start:idx_end+1]],
                     info, names, units)
    del(ni_30)

    print "8.0 degree"
    ni_80=mr.McSim('../results/nickle_10x10_80/mccode.h5')['tof_detector']
    q, I, R=eh.calcR(ni_80, ref, 8.0, 1.0, detcorr=True, **opts)
    q, I_real, R_real=eh.calcR(ni_80, ref, 8.0, 1.0, use_tof=False, **opts)
    idx_start, idx_end=where((I>0.)&(I_real>0.))[0][[0,-1]]
    eh.save_w_header('../results/analyzed/reflectivity_10x10_80.dat',
                     [q[idx_start:idx_end+1],
                      I[idx_start:idx_end+1], R[idx_start:idx_end+1],
                      (R/sqrt(I))[idx_start:idx_end+1],
                      I_real[idx_start:idx_end+1], R_real[idx_start:idx_end+1]],
                     info, names, units)

  if 4 in only_items:
    print "10x10 sample pulse skipping mode."
    opts=dict(qres=0.04, qmin=0.003, qmax=0.2, mindq=2e-4)
    names=['q_z', 'Intensity(ToF)', 'Reflectivity(ToF)', 'Intensity(λ)', 'Reflectivity(λ)']
    units=['A^{-1}', 'cts/s', '1', 'cts/s', '1']
    info='Reflectivity simulation for 10x10mm² sample, 2-pulse skipping'

    ref=mr.McSim('../results/single_skip_reference/mccode.h5')['tof_detector']
    ni=mr.McSim('../results/single_skip_nickle/mccode.h5')['tof_detector']
    q, I, R=eh.calcR(ni, ref, 2.0, skip_pulses=2, **opts)
    q, I_real, R_real=eh.calcR(ni, ref, 2.0, skip_pulses=2, use_tof=False, **opts)
    idx_start, idx_end=where((I>0.))[0][[0,-1]]
    eh.save_w_header('../results/analyzed/single_skip_reflectivity.dat',
                     [q[idx_start:idx_end+1],
                      I[idx_start:idx_end+1], R[idx_start:idx_end+1],
                      I_real[idx_start:idx_end+1], R_real[idx_start:idx_end+1]],
                     info, names, units)

  if 5 in only_items:
    print "Extracting event statistics."
    fh=open('../results/analyzed/event_stats.dat', 'w')
    fh.write('# event statistics for Estia\n')
    fh.write('# item      avg. total     peak total     avg. ROI       peak ROI\n')
    fh.write('#           [cps]          [cps]          [cps/mm²]      [cps/mm²]\n')
    line='%(name)s  %(total) 12e  %(peak) 12e  %(roi) 12e  %(roi_peak) 12e\n'

    ds=mr.McSim('../results/reference_10x10_10/mccode.h5')['tof_detector']
    res=eh.calcStat(ds)
    res['name']='Reference'
    fh.write(line%res)

    ds=mr.McSim('../results/nickle_10x10_08/mccode.h5')['tof_detector']
    res=eh.calcStat(ds)
    res['name']='Ni-0.8deg'
    fh.write(line%res)

    ds=mr.McSim('../results/nickle_10x10_30/mccode.h5')['tof_detector']
    res=eh.calcStat(ds)
    res['name']='Ni-3.0deg'
    fh.write(line%res)

    ds=mr.McSim('../results/nickle_10x10_80/mccode.h5')['tof_detector']
    res=eh.calcStat(ds)
    res['name']='Ni-8.0deg'
    fh.write(line%res)

    fh.close()
    
