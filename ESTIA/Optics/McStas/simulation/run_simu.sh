#!/bin/bash

DEST=../results
ncount=1e9
use_cores=6
sample=4
omega=0.8
sample_length=0.005
sample_height=0.01
lambda_start=3.0
lambda_end=32.0

###################### Brilliance Transfer 10x10mm² and 1x1mm² VS ####################
bash compile_if_needed.sh

DESTi=$DEST/brilliance_nowindow_5x10
if [ -e "$DESTi" ]; then
	rm -r "$DESTi"
fi

mpirun  -np $use_cores Estia_baseline.out \
        --dir="$DESTi" --format=NeXuS --ncount=$ncount --gravitation \
        omegaa=$omega operationmode=0 theta_resolution=0.04 over_illumination=0.000 \
        sample=$sample sample_length=$sample_length sample_height=$sample_height \
        enable_windows=0 \
        lambda_start=$lambda_start lambda_end=$lambda_end enable_gravity=1 enable_chopper=0

DESTi=$DEST/brilliance_5x10
if [ -e "$DESTi" ]; then
	rm -r "$DESTi"
fi

mpirun  -np $use_cores Estia_baseline.out \
        --dir="$DESTi" --format=NeXuS --ncount=$ncount --gravitation \
        omegaa=$omega operationmode=0 theta_resolution=0.04 over_illumination=0.000 \
        sample=$sample sample_length=$sample_length sample_height=$sample_height \
        lambda_start=$lambda_start lambda_end=$lambda_end enable_gravity=1 enable_chopper=0


ncount=1e10
sample_length=0.001
sample_height=0.001

DESTi=$DEST/brilliance_1x1
if [ -e "$DESTi" ]; then
	rm -r "$DESTi"
fi

mpirun  -np $use_cores Estia_baseline.out \
        --dir="$DESTi" --format=NeXuS --ncount=$ncount --gravitation \
        omegaa=$omega operationmode=0 theta_resolution=0.04 over_illumination=0.000 \
        sample=$sample sample_length=$sample_length sample_height=$sample_height \
        lambda_start=$lambda_start lambda_end=$lambda_end enable_gravity=1 enable_chopper=0


# ###################### Reference and Ni-layer measurement 10x10mm² sample ####################
ncount=1e10
sample_length=0.01
sample_height=0.01
lambda_start=3.25
lambda_end=12.75
sample=1

omega=1.0
DESTi=$DEST/reference_10x10_10
if [ -e "$DESTi" ]; then
	rm -r "$DESTi"
fi

mpirun  -np $use_cores Estia_baseline.out \
        --dir="$DESTi" --format=NeXuS --ncount=$ncount --gravitation \
        omegaa=$omega operationmode=0 theta_resolution=0.04 over_illumination=0.000 \
        sample=$sample sample_length=$sample_length sample_height=$sample_height \
        lambda_start=$lambda_start lambda_end=$lambda_end enable_gravity=1 enable_chopper=1
       
#     Ni Sample
        
ncount=6e9
sample=2
omega=0.8
DESTi=$DEST/nickle_10x10_08
if [ -e "$DESTi" ]; then
	rm -r "$DESTi"
fi

mpirun  -np $use_cores Estia_baseline.out \
        --dir="$DESTi" --format=NeXuS --ncount=$ncount --gravitation \
        omegaa=$omega operationmode=0 theta_resolution=0.04 over_illumination=0.0001 \
        sample=$sample sample_length=$sample_length sample_height=$sample_height \
        lambda_start=$lambda_start lambda_end=$lambda_end enable_gravity=1 enable_chopper=1

ncount=2e9
omega=3.0
DESTi=$DEST/nickle_10x10_30
if [ -e "$DESTi" ]; then
	rm -r "$DESTi"
fi

mpirun  -np $use_cores Estia_baseline.out \
        --dir="$DESTi" --format=NeXuS --ncount=$ncount --gravitation \
        omegaa=$omega operationmode=0 theta_resolution=0.04 over_illumination=0.0001 \
        sample=$sample sample_length=$sample_length sample_height=$sample_height \
        lambda_start=$lambda_start lambda_end=$lambda_end enable_gravity=1 enable_chopper=1
        
        
ncount=1e9
omega=8.0
DESTi=$DEST/nickle_10x10_80
if [ -e "$DESTi" ]; then
	rm -r "$DESTi"
fi

mpirun  -np $use_cores Estia_baseline.out \
        --dir="$DESTi" --format=NeXuS --ncount=$ncount --gravitation \
        omegaa=$omega operationmode=0 theta_resolution=0.04 over_illumination=0.0001 \
        sample=$sample sample_length=$sample_length sample_height=$sample_height \
        lambda_start=$lambda_start lambda_end=$lambda_end enable_gravity=1 enable_chopper=1

# ###################### Reference and Ni-layer measurement 1x1mm² sample ####################
# 
# 
