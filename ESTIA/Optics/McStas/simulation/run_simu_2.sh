#!/bin/bash

DEST=../results
use_cores=6

######################  Compile model if necessary   ####################
bash compile_if_neede.sh

######## Reference and Ni-layer conventional measurement 10x10mm² sample #############
ncount=1e10
sample_length=0.01
sample_height=0.01
lambda_start=3.25
lambda_min=3.75
lambda_end=12.75
sample=1

omega=1.0
DESTi=$DEST/tof_reference_10x10_10
if [ -e "$DESTi" ]; then
	rm -r "$DESTi"
fi

mpirun  -np $use_cores Estia_baseline.out \
        --dir="$DESTi" --format=NeXuS --ncount=$ncount --gravitation \
        omegaa=$omega operationmode=1 theta_resolution=0.01 over_illumination=0.0001 \
        sample=$sample sample_length=$sample_length sample_height=$sample_height \
        lambda_start=$lambda_start lambda_min=$lambda_min lambda_end=$lambda_end enable_gravity=1 enable_chopper=1
       
#     Ni Sample
ncount=2e10
sample=2
omega=0.5
DESTi=$DEST/tof_nickle_10x10_05
if [ -e "$DESTi" ]; then
	rm -r "$DESTi"
fi

mpirun  -np $use_cores Estia_baseline.out \
        --dir="$DESTi" --format=NeXuS --ncount=$ncount --gravitation \
        omegaa=$omega operationmode=1 theta_resolution=0.03 over_illumination=0.0001 \
        sample=$sample sample_length=$sample_length sample_height=$sample_height \
        lambda_start=$lambda_start lambda_min=$lambda_min lambda_end=$lambda_end enable_gravity=1 enable_chopper=1

ncount=1e10
omega=1.2
DESTi=$DEST/tof_nickle_10x10_12
if [ -e "$DESTi" ]; then
	rm -r "$DESTi"
fi

mpirun  -np $use_cores Estia_baseline.out \
        --dir="$DESTi" --format=NeXuS --ncount=$ncount --gravitation \
        omegaa=$omega operationmode=1 theta_resolution=0.03 over_illumination=0.0001 \
        sample=$sample sample_length=$sample_length sample_height=$sample_height \
        lambda_start=$lambda_start lambda_min=$lambda_min lambda_end=$lambda_end enable_gravity=1 enable_chopper=1
        
ncount=5e9
omega=3.0
DESTi=$DEST/tof_nickle_10x10_30
if [ -e "$DESTi" ]; then
	rm -r "$DESTi"
fi

mpirun  -np $use_cores Estia_baseline.out \
        --dir="$DESTi" --format=NeXuS --ncount=$ncount --gravitation \
        omegaa=$omega operationmode=1 theta_resolution=0.03 over_illumination=0.0001 \
        sample=$sample sample_length=$sample_length sample_height=$sample_height \
        lambda_start=$lambda_start lambda_min=$lambda_min lambda_end=$lambda_end enable_gravity=1 enable_chopper=1
                
ncount=2e9
omega=7.5
DESTi=$DEST/tof_nickle_10x10_75
if [ -e "$DESTi" ]; then
	rm -r "$DESTi"
fi

mpirun  -np $use_cores Estia_baseline.out \
        --dir="$DESTi" --format=NeXuS --ncount=$ncount --gravitation \
        omegaa=$omega operationmode=1 theta_resolution=0.03 over_illumination=0.0001 \
        sample=$sample sample_length=$sample_length sample_height=$sample_height \
        lambda_start=$lambda_start lambda_min=$lambda_min lambda_end=$lambda_end enable_gravity=1 enable_chopper=1

###################### Reference and Ni-layer measurement 1x1mm² sample ####################


