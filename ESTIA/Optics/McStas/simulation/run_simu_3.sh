#!/bin/bash

DEST=../results
ncount=1e9
use_cores=6
sample=4
omega=0.8
sample_length=0.01
sample_height=0.01
lambda_start=3.0
lambda_end=32.0

bash compile_if_neede.sh


################# Reference and Ni-layer measurement 2 pulse skipping ####################
ncount=4e9
sample_length=0.01
sample_height=0.01
lambda_start=3.9
lambda_end=24.0
sample=1
omega=2.0

DESTi=$DEST/single_skip_reference
if [ -e "$DESTi" ]; then
	rm -r "$DESTi"
fi

mpirun  -np $use_cores Estia_baseline.out \
        --dir="$DESTi" --format=NeXuS --ncount=$ncount --gravitation \
        omegaa=$omega operationmode=0 theta_resolution=0.04 over_illumination=0.0002 \
        sample=$sample sample_length=$sample_length sample_height=$sample_height \
        lambda_start=$lambda_start lambda_end=$lambda_end enable_gravity=1 enable_chopper=3
        
sample=2
DESTi=$DEST/single_skip_nickle
if [ -e "$DESTi" ]; then
	rm -r "$DESTi"
fi

mpirun  -np $use_cores Estia_baseline.out \
        --dir="$DESTi" --format=NeXuS --ncount=$ncount --gravitation \
        omegaa=$omega operationmode=0 theta_resolution=0.04 over_illumination=0.0002 \
        sample=$sample sample_length=$sample_length sample_height=$sample_height \
        lambda_start=$lambda_start lambda_end=$lambda_end enable_gravity=1 enable_chopper=3
