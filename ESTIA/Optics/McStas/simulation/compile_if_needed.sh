#!/bin/bash

if [ Estia_baseline.instr -nt Estia_baseline.out ] || [ ! -f Estia_baseline.out ] \
   || [ Estia_feeder.instr -nt Estia_baseline.out ] \
   || [ Estia_selene.instr -nt Estia_baseline.out ]; then
	rm Estia_baseline.c Estia_baseline.out
	mcstas -o Estia_baseline.c Estia_baseline.instr
	mpicc -O3 -o Estia_baseline.out Estia_baseline.c -lm -DUSE_MPI -DUSE_NEXUS -lNeXus
fi
