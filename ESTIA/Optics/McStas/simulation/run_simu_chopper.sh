#!/bin/bash

DEST=../results
ncount=1e9
use_cores=6
sample=1
omega=15.0
sample_length=0.02
sample_height=0.01

lambda_start=3.5
lambda_min=5.0
lambda_end=15.5
frame_usage=0.92

###################### Brilliance Transfer 10x10mm² and 1x1mm² VS ####################
bash compile_if_neede.sh

###################### Reference and Ni-layer measurement 10x10mm² sample ####################

frame_usage=0.92
DESTi=$DEST/chopper_f1_092
if [ -e "$DESTi" ]; then
	rm -r "$DESTi"
fi

mpirun  -np $use_cores Estia_baseline.out \
        --dir="$DESTi" --format=NeXuS --ncount=$ncount \
        omegaa=$omega operationmode=0 theta_resolution=0.04 over_illumination=0.0001 \
        sample=$sample sample_length=$sample_length sample_height=$sample_height \
        lambda_start=$lambda_start lambda_end=$lambda_end enable_gravity=0 enable_chopper=1 \
        frame_usage=$frame_usage lambda_min=$lambda_min
       
frame_usage=0.94
DESTi=$DEST/chopper_f1_094
if [ -e "$DESTi" ]; then
	rm -r "$DESTi"
fi

mpirun  -np $use_cores Estia_baseline.out \
        --dir="$DESTi" --format=NeXuS --ncount=$ncount \
        omegaa=$omega operationmode=0 theta_resolution=0.04 over_illumination=0.0001 \
        sample=$sample sample_length=$sample_length sample_height=$sample_height \
        lambda_start=$lambda_start lambda_end=$lambda_end enable_gravity=0 enable_chopper=1 \
        frame_usage=$frame_usage lambda_min=$lambda_min
       
frame_usage=0.96
DESTi=$DEST/chopper_f1_096
if [ -e "$DESTi" ]; then
	rm -r "$DESTi"
fi

mpirun  -np $use_cores Estia_baseline.out \
        --dir="$DESTi" --format=NeXuS --ncount=$ncount \
        omegaa=$omega operationmode=0 theta_resolution=0.04 over_illumination=0.0001 \
        sample=$sample sample_length=$sample_length sample_height=$sample_height \
        lambda_start=$lambda_start lambda_end=$lambda_end enable_gravity=0 enable_chopper=1 \
        frame_usage=$frame_usage lambda_min=$lambda_min
       
frame_usage=0.974
DESTi=$DEST/chopper_f1_097s
if [ -e "$DESTi" ]; then
	rm -r "$DESTi"
fi

mpirun  -np $use_cores Estia_baseline.out \
        --dir="$DESTi" --format=NeXuS --ncount=$ncount \
        omegaa=$omega operationmode=0 theta_resolution=0.04 over_illumination=0.0001 \
        sample=$sample sample_length=$sample_length sample_height=$sample_height \
        lambda_start=$lambda_start lambda_end=$lambda_end enable_gravity=0 enable_chopper=1 \
        frame_usage=$frame_usage lambda_min=$lambda_min
       
frame_usage=0.98
DESTi=$DEST/chopper_f1_098
if [ -e "$DESTi" ]; then
	rm -r "$DESTi"
fi

mpirun  -np $use_cores Estia_baseline.out \
        --dir="$DESTi" --format=NeXuS --ncount=$ncount \
        omegaa=$omega operationmode=0 theta_resolution=0.04 over_illumination=0.0001 \
        sample=$sample sample_length=$sample_length sample_height=$sample_height \
        lambda_start=$lambda_start lambda_end=$lambda_end enable_gravity=0 enable_chopper=1 \
        frame_usage=$frame_usage lambda_min=$lambda_min
       
frame_usage=0.99
DESTi=$DEST/chopper_f1_099
if [ -e "$DESTi" ]; then
	rm -r "$DESTi"
fi

mpirun  -np $use_cores Estia_baseline.out \
        --dir="$DESTi" --format=NeXuS --ncount=$ncount \
        omegaa=$omega operationmode=0 theta_resolution=0.04 over_illumination=0.0001 \
        sample=$sample sample_length=$sample_length sample_height=$sample_height \
        lambda_start=$lambda_start lambda_end=$lambda_end enable_gravity=0 enable_chopper=1 \
        frame_usage=$frame_usage lambda_min=$lambda_min
       
       
frame_usage=0.98
lambda_start=2.75
lambda_min=3.75
lambda_end=12.75
DESTi=$DEST/chopper_f1_375A
if [ -e "$DESTi" ]; then
	rm -r "$DESTi"
fi

mpirun  -np $use_cores Estia_baseline.out \
        --dir="$DESTi" --format=NeXuS --ncount=$ncount \
        omegaa=$omega operationmode=0 theta_resolution=0.04 over_illumination=0.0001 \
        sample=$sample sample_length=$sample_length sample_height=$sample_height \
        lambda_start=$lambda_start lambda_end=$lambda_end enable_gravity=0 enable_chopper=1 \
        frame_usage=$frame_usage lambda_min=$lambda_min
       
       
###################### Reference and Ni-layer measurement 1x1mm² sample ####################


