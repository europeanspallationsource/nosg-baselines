#!/bin/bash

DEST=../results
ncount=2e7
use_cores=4
sample=4
omega=0.8
sample_length=0.01
sample_height=0.01
lambda_start=3.0
lambda_end=32.0

###################### Brilliance Transfer 10x10mm² and 1x1mm² VS ####################
if [ Estia_baseline.instr -nt Estia_baseline.out ]; then
	rm Estia_baseline.c Estia_baseline.out
	mcstas -o Estia_baseline.c Estia_baseline.instr
	mpicc -O3 -o Estia_baseline.out Estia_baseline.c -lm -DUSE_MPI -DUSE_NEXUS -lNeXus
fi
# 
# if [ Estia_baseline_ana1.instr -nt Estia_baseline_ana1.out ]; then
# 	rm Estia_baseline_ana1.c Estia_baseline_ana1.out
# 	mcstas -o Estia_baseline_ana1.c Estia_baseline_ana1.instr
# 	mpicc -O3 -o Estia_baseline_ana1.out Estia_baseline_ana1.c -lm -DUSE_MPI -DUSE_NEXUS -lNeXus
# fi
# 
# if [ Estia_baseline_ana2.instr -nt Estia_baseline_ana2.out ]; then
# 	rm Estia_baseline_ana2.c Estia_baseline_ana2.out
# 	mcstas -o Estia_baseline_ana2.c Estia_baseline_ana2.instr
# 	mpicc -O3 -o Estia_baseline_ana2.out Estia_baseline_ana2.c -lm -DUSE_MPI -DUSE_NEXUS -lNeXus
# fi


###################### Reference and Ni-layer measurement 10x10mm² sample ####################
# ncount=1e10
sample_length=0.01
sample_height=0.01
lambda_start=3.5
lambda_end=30.0
sample=1

# omega=1.0
# DESTi=$DEST/pol_ref_10x10_10
# if [ -e "$DESTi" ]; then
# 	rm -r "$DESTi"
# fi
# 
# mpirun  -np $use_cores Estia_baseline.out \
#         --dir="$DESTi" --ncount=$ncount --gravitation \
#         omegaa=$omega operationmode=0 theta_resolution=0.04 over_illumination=0.000 \
#         sample=$sample sample_length=$sample_length sample_height=$sample_height \
#         lambda_start=$lambda_start lambda_end=$lambda_end enable_gravity=1 enable_chopper=0 enable_polarizer=0 enable_analyzer=0
# 
# DESTi=$DEST/pol1_10x10_10
# if [ -e "$DESTi" ]; then
# 	rm -r "$DESTi"
# fi
# 
# mpirun  -np $use_cores Estia_baseline.out \
#         --dir="$DESTi" --ncount=$ncount --gravitation \
#         omegaa=$omega operationmode=0 theta_resolution=0.04 over_illumination=0.000 \
#         sample=$sample sample_length=$sample_length sample_height=$sample_height \
#         lambda_start=$lambda_start lambda_end=$lambda_end enable_gravity=1 enable_chopper=0 enable_polarizer=1 enable_analyzer=0
# 
# DESTi=$DEST/pol2_10x10_10
# if [ -e "$DESTi" ]; then
# 	rm -r "$DESTi"
# fi
# 
# mpirun  -np $use_cores Estia_baseline.out \
#         --dir="$DESTi" --ncount=$ncount --gravitation \
#         omegaa=$omega operationmode=0 theta_resolution=0.04 over_illumination=0.000 \
#         sample=$sample sample_length=$sample_length sample_height=$sample_height \
#         lambda_start=$lambda_start lambda_end=$lambda_end enable_gravity=1 enable_chopper=0 enable_polarizer=2 enable_analyzer=0
# # 
# omega=7.0
# DESTi=$DEST/pol1_10x10_70
# if [ -e "$DESTi" ]; then
# 	rm -r "$DESTi"
# fi
# 
# mpirun  -np $use_cores Estia_baseline.out \
#         --dir="$DESTi" --ncount=$ncount --gravitation \
#         omegaa=$omega operationmode=0 theta_resolution=0.04 over_illumination=0.000 \
#         sample=$sample sample_length=$sample_length sample_height=$sample_height \
#         lambda_start=$lambda_start lambda_end=$lambda_end enable_gravity=1 enable_chopper=0 enable_polarizer=1 enable_analyzer=0
# 
# DESTi=$DEST/pol2_10x10_70
# if [ -e "$DESTi" ]; then
# 	rm -r "$DESTi"
# fi
# 
# mpirun  -np $use_cores Estia_baseline.out \
#         --dir="$DESTi" --ncount=$ncount --gravitation \
#         omegaa=$omega operationmode=0 theta_resolution=0.04 over_illumination=0.000 \
#         sample=$sample sample_length=$sample_length sample_height=$sample_height \
#         lambda_start=$lambda_start lambda_end=$lambda_end enable_gravity=1 enable_chopper=0 enable_polarizer=2 enable_analyzer=0
# 

# ncount=1e8
omega=0.0
# DESTi=$DEST/pol1_10x50_70
# if [ -e "$DESTi" ]; then
# 	rm -r "$DESTi"
# fi
# 
# mpirun  -np $use_cores Estia_baseline.out \
#         --dir="$DESTi" --ncount=$ncount --gravitation \
#         omegaa=$omega operationmode=0 theta_resolution=0.04 over_illumination=0.0025 \
#         sample=$sample sample_length=$sample_length sample_height=$sample_height \
#         lambda_start=$lambda_start lambda_end=$lambda_end enable_gravity=1 enable_chopper=0 enable_polarizer=1 enable_analyzer=0

DESTi=$DEST/pol2_10x50_70
if [ -e "$DESTi" ]; then
	rm -r "$DESTi"
fi

mpirun  -np $use_cores Estia_baseline.out \
        --dir="$DESTi" --ncount=$ncount --gravitation \
        omegaa=$omega operationmode=0 theta_resolution=0.04 over_illumination=0.0025 \
        sample=$sample sample_length=$sample_length sample_height=$sample_height \
        lambda_start=$lambda_start lambda_end=$lambda_end enable_gravity=1 enable_chopper=0 enable_polarizer=2 enable_analyzer=0
# 
# ncount=5e9
# sample_length=0.01
# omega=1.0
# sample_length=0.003
# DESTi=$DEST/pol_ref_3x10_10
# if [ -e "$DESTi" ]; then
# 	rm -r "$DESTi"
# fi
# 
# mpirun  -np $use_cores Estia_baseline.out \
#         --dir="$DESTi" --ncount=$ncount --gravitation \
#         omegaa=$omega operationmode=0 theta_resolution=0.04 over_illumination=0.000 \
#         sample=$sample sample_length=$sample_length sample_height=$sample_height \
#         lambda_start=$lambda_start lambda_end=$lambda_end enable_gravity=1 enable_chopper=0 enable_polarizer=0 enable_analyzer=0
# 
# DESTi=$DEST/pol1_3x10_10
# if [ -e "$DESTi" ]; then
# 	rm -r "$DESTi"
# fi
# 
# mpirun  -np $use_cores Estia_baseline.out \
#         --dir="$DESTi" --ncount=$ncount --gravitation \
#         omegaa=$omega operationmode=0 theta_resolution=0.04 over_illumination=0.000 \
#         sample=$sample sample_length=$sample_length sample_height=$sample_height \
#         lambda_start=$lambda_start lambda_end=$lambda_end enable_gravity=1 enable_chopper=0 enable_polarizer=1 enable_analyzer=0
# 
# DESTi=$DEST/pol2_3x10_10
# if [ -e "$DESTi" ]; then
# 	rm -r "$DESTi"
# fi
# 
# mpirun  -np $use_cores Estia_baseline.out \
#         --dir="$DESTi" --ncount=$ncount --gravitation \
#         omegaa=$omega operationmode=0 theta_resolution=0.04 over_illumination=0.000 \
#         sample=$sample sample_length=$sample_length sample_height=$sample_height \
#         lambda_start=$lambda_start lambda_end=$lambda_end enable_gravity=1 enable_chopper=0 enable_polarizer=2 enable_analyzer=0
# 
# 
# sample_height=0.01
# sample_length=0.01
# DESTi=$DEST/ana1_10x10_10
# if [ -e "$DESTi" ]; then
# 	rm -r "$DESTi"
# fi
# 
# mpirun  -np $use_cores Estia_baseline.out \
#         --dir="$DESTi" --ncount=$ncount --gravitation \
#         omegaa=$omega operationmode=0 theta_resolution=0.04 over_illumination=0.000 \
#         sample=$sample sample_length=$sample_length sample_height=$sample_height \
#         lambda_start=$lambda_start lambda_end=$lambda_end enable_gravity=1 enable_chopper=0 enable_polarizer=0 enable_analyzer=1

# sample_height=0.001
# DESTi=$DEST/ana1_10x1_10
# if [ -e "$DESTi" ]; then
# 	rm -r "$DESTi"
# fi
# 
# mpirun  -np $use_cores Estia_baseline_ana1.out \
#         --dir="$DESTi" --ncount=$ncount --gravitation \
#         omegaa=$omega operationmode=0 theta_resolution=0.04 over_illumination=0.000 \
#         sample=$sample sample_length=$sample_length sample_height=$sample_height \
#         lambda_start=$lambda_start lambda_end=$lambda_end enable_gravity=1 enable_chopper=0 enable_polarizer=0 enable_analyzer=1
#        
# sample_height=0.01
# DESTi=$DEST/ana2_10x10_10
# if [ -e "$DESTi" ]; then
# 	rm -r "$DESTi"
# fi
# 
# mpirun  -np $use_cores Estia_baseline.out \
#         --dir="$DESTi" --ncount=$ncount --gravitation \
#         omegaa=$omega operationmode=0 theta_resolution=0.04 over_illumination=0.000 \
#         sample=$sample sample_length=$sample_length sample_height=$sample_height \
#         lambda_start=$lambda_start lambda_end=$lambda_end enable_gravity=1 enable_chopper=0 enable_polarizer=0 enable_analyzer=2

# sample_height=0.001
# DESTi=$DEST/ana2_10x1_10
# if [ -e "$DESTi" ]; then
# 	rm -r "$DESTi"
# fi
# 
# mpirun  -np $use_cores Estia_baseline_ana2.out \
#         --dir="$DESTi" --ncount=$ncount --gravitation \
#         omegaa=$omega operationmode=0 theta_resolution=0.04 over_illumination=0.000 \
#         sample=$sample sample_length=$sample_length sample_height=$sample_height \
#         lambda_start=$lambda_start lambda_end=$lambda_end enable_gravity=1 enable_chopper=0 enable_polarizer=0 enable_analyzer=2
       

###################### Reference and Ni-layer measurement 1x1mm² sample ####################


