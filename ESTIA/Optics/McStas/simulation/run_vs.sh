#!/bin/bash

DEST=../results
ncount=1e8
use_cores=6
sample=1
omega=0.8
sample_length=0.01
sample_height=0.015

if [ Estia_vs.instr -nt Estia_vs.out ]; then
	rm Estia_vs.c Estia_vs.out
	mcstas -t -o Estia_vs.c Estia_vs.instr
	mpicc -O3 -o Estia_vs.out Estia_vs.c -lm -DUSE_MPI -DUSE_NEXUS -lNeXus
fi


omega=2.0
DESTi=$DEST/vs_data
if [ -e "$DESTi" ]; then
	rm -r "$DESTi"
fi

mpirun  -np $use_cores Estia_vs.out \
        --dir="$DESTi" --format=NeXuS --ncount=$ncount --gravitation \
        omegaa=$omega operationmode=0 theta_resolution=0.04 over_illumination=0.0025 \
        sample=$sample sample_length=$sample_length sample_height=$sample_height \
        enable_gravity=1 enable_chopper=0 source_power=5
       
###################### Reference and Ni-layer measurement 1x1mm² sample ####################


