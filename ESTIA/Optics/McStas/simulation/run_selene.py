#!/usr/bin/env python
#-*- coding: utf8 -*-

import sys, os
import numpy as np
from subprocess import call

FOLDER='../results2/'
PREFIX='selene_geo_'
SEED=3798
ITEMS=1000

CALL='mpirun -np 6 Selene_geometry.out --dir=%s --ncount=4e8 enable_gravity=0 sample_length=0.003 '
CALL+='tx_1=%f tz_1=%f ry_1=%f rz_1=%f tx_2=%f tz_2=%f ry_2=%f rz_2=%f '

TX=[-0.001, 0.001]
TZ=[-0.005, 0.005]
RY=[-0.001, 0.001]
RZ=[-2.5, 2.5]


if __name__=='__main__':
    call('mcstas -t -o Selene_geometry.c Selene_geometry.instr'.split())
    call('mpicc -O2 -o Selene_geometry.out Selene_geometry.c -lm -DUSE_MPI'.split())
    
    np.random.seed(SEED)
    fh=open(os.path.join(FOLDER, PREFIX+'sims.dat'), 'w')
    fh.write('# index tx_1 tz_1 ry_1 rz_1 tx_2 tz_2 ry_2 rz_2\n')
    fh.write('# seed = %i\n'%SEED)
    
    for i in range(ITEMS):
        tx_1=np.random.random()*(TX[1]-TX[0])+TX[0]
        tx_2=np.random.random()*(TX[1]-TX[0])+TX[0]
        tz_1=np.random.random()*(TZ[1]-TZ[0])+TZ[0]
        tz_2=np.random.random()*(TZ[1]-TZ[0])+TZ[0]
        ry_1=np.random.random()*(RY[1]-RY[0])+RY[0]
        ry_2=np.random.random()*(RY[1]-RY[0])+RY[0]
        rz_1=np.random.random()*(RZ[1]-RZ[0])+RZ[0]
        rz_2=np.random.random()*(RZ[1]-RZ[0])+RZ[0]
        ln=' '.join(['%f'%val for val in [tx_1, tz_1, ry_1, rz_1, tx_2, tz_2, ry_2, rz_2]])
        fh.write(ln+'\n')
        print i, ':', ln
        call((CALL%(os.path.join(FOLDER, PREFIX+'%05i/'%i), tx_1, tz_1, ry_1, rz_1, tx_2, tz_2, ry_2, rz_2)).split())
    
    fh.close()
    
