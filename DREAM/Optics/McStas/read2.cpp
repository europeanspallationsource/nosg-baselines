#include <fstream>
#include <iostream>
#include <vector>
#include <string>

int main()
{

  std::cout << "// Auto-generated output - do not edit! " << std::endl ;
  std::cout << "// Should be placed in Guide_declarations.c to be %included in the DREAM instr file (DECLARE) " << std::endl << std::endl;
  const std::string filename1 = "guide_1.txt";
  std::fstream infile1(filename1, std::ios::in);
  
  if (!infile1.is_open()) {
    std::cout<<"File not found!"<<std::endl;
    return 1;
  }
  
  // Read the first two lines

  std::string aline;
  for(int iline=0; iline <2; iline++)
    getline(infile1,aline);

  std::vector<double> sides;
  std::vector<double> bottoms;
  std::vector<double> lengths;
  sides.clear();
  bottoms.clear();
  
  while(!infile1.eof()){
    double l, w, h, rl, tb;
    std::string refl, fname, left, right; 
    infile1 >> l >> w >> h >> refl >> fname >> left >> right >> rl >> tb ;
    sides.push_back(rl);
    bottoms.push_back(tb);
  }
  std::cout << "// Declarations for guide: " << filename1 << ", number of elements is " << sides.size() << std::endl ;
  size_t i;
  std::cout << "double side_m_array1[] = {";
  for(i=0; i< sides.size()-1; i++) 
    std::cout << sides[i] << ", ";
  std::cout << sides[sides.size()-1] << "};" <<std::endl ;

  std::cout << "double bt_m_array1[]   = {";
  for(i=0; i< bottoms.size()-1; i++) 
    std::cout << bottoms[i] << ", ";
  std::cout << bottoms[bottoms.size()-1] << "};" <<std::endl <<std::endl;

  std::cout << "double seg_array1[]   = {";
  for(i=0; i< bottoms.size()-1; i++) 
    std::cout << 1.931/bottoms.size() << ", ";
  std::cout << 1.931/bottoms.size() << "};" <<std::endl <<std::endl;

  
  const std::string filename2 = "guide_2.txt";
  std::fstream infile2(filename2, std::ios::in);

  
  if (!infile2.is_open()) {
    std::cout<<"File not found!"<<std::endl;
    return 1;
  }
  
  // Read the first two lines

  std::string aline2;
  for(int iline=0; iline <2; iline++)
    getline(infile2,aline2);

  //std::vector<double> sides;
  //std::vector<double> bottoms;
  sides.clear();
  bottoms.clear();
  
  while(!infile2.eof()){
    double l, w, h, rl, tb;
    std::string refl, fname, left, right; 
    infile2 >> l >> w >> h >> refl >> fname >> left >> right >> rl >> tb ;
    sides.push_back(rl);
    bottoms.push_back(tb);
  }
  std::cout << "// Declarations for guide: " << filename2 << ", number of elements is " << sides.size() << std::endl ;

  std::cout << "double side_m_array2[] = {";
  for(i=0; i< sides.size()-1; i++) 
    std::cout << sides[i] << ", ";
  std::cout << sides[sides.size()-1] << "};" <<std::endl ;

  std::cout << "double bt_m_array2[]   = {";
  for(i=0; i< bottoms.size()-1; i++) 
    std::cout << bottoms[i] << ", ";
  std::cout << bottoms[bottoms.size()-1] << "};" <<std::endl <<std::endl;

  std::cout << "double seg_array2[]   = {";
  for(i=0; i< bottoms.size()-1; i++) 
    std::cout << 3.9456/bottoms.size() << ", ";
  std::cout << 3.9456/bottoms.size() << "};" <<std::endl <<std::endl;

  const std::string filename3 = "guide_3.txt";
  std::fstream infile3(filename3, std::ios::in);

  
  if (!infile3.is_open()) {
    std::cout<<"File not found!"<<std::endl;
    return 1;
  }
  
  // Read the first two lines

  std::string aline3;
  for(int iline=0; iline <3; iline++)
    getline(infile3,aline3);

  //std::vector<double> sides;
  //std::vector<double> bottoms;
  sides.clear();
  bottoms.clear();
  
  while(!infile3.eof()){
    double l, w, h, rl, tb;
    std::string refl, fname, left, right; 
    infile3 >> l >> w >> h >> refl >> fname >> left >> right >> rl >> tb ;
    sides.push_back(rl);
    bottoms.push_back(tb);
  }
  std::cout << "// Declarations for guide: " << filename3 << ", number of elements is " << sides.size() << std::endl ;

  std::cout << "double side_m_array3[] = {";
  for(i=0; i< sides.size()-1; i++) 
    std::cout << sides[i] << ", ";
  std::cout << sides[sides.size()-1] << "};" <<std::endl ;

  std::cout << "double bt_m_array3[]   = {";
  for(i=0; i< bottoms.size()-1; i++) 
    std::cout << bottoms[i] << ", ";
  std::cout << bottoms[bottoms.size()-1] << "};" <<std::endl <<std::endl;

  std::cout << "double seg_array3[]   = {";
  for(i=0; i< bottoms.size()-1; i++) 
    std::cout << 10.0738/bottoms.size() << ", ";
  std::cout << 10.0738/bottoms.size() << "};" <<std::endl <<std::endl;


  const std::string filename5 = "guide_5.txt";
  std::fstream infile5(filename5, std::ios::in);

  
  if (!infile5.is_open()) {
    std::cout<<"File not found!"<<std::endl;
    return 1;
  }
  
  // Read the first two lines

  std::string aline5;
  for(int iline=0; iline <5; iline++)
    getline(infile5,aline5);

  //std::vector<double> sides;
  //std::vector<double> bottoms;
  sides.clear();
  bottoms.clear();
  
  while(!infile5.eof()){ 
    double l, w, h, rl, tb;
    std::string refl, fname, left, right; 
    infile5 >> l >> w >> h >> refl >> fname >> left >> right >> rl >> tb ;
    sides.push_back(rl);
    bottoms.push_back(tb);
  }
  std::cout << "// Declarations for guide: " << filename5 << ", number of elements is " << sides.size() << std::endl ;

  std::cout << "double side_m_array5[] = {";
  for(i=0; i< sides.size()-1; i++) 
    std::cout << sides[i] << ", ";
  std::cout << sides[sides.size()-1] << "};" <<std::endl ;

  std::cout << "double bt_m_array5[]   = {";
  for(i=0; i< bottoms.size()-1; i++) 
    std::cout << bottoms[i] << ", ";
  std::cout << bottoms[bottoms.size()-1] << "};" <<std::endl <<std::endl;

  std::cout << "double seg_array5[]   = {";
  for(i=0; i< bottoms.size()-1; i++) 
    std::cout << 16.567/bottoms.size() << ", ";
  std::cout << 16.567/bottoms.size() << "};" <<std::endl <<std::endl;

  return 0;
}
