/*******************************************************************************
* Instrument: ESS_DREAM_LIKE
*
* %I
* Written by: Peter Willendrup <peter.willendrup@esss.se>
* Date: 201701
* Origin: ESS
* Release: McStas 2.4
* Version: 0.1
* %INSTRUMENT_SITE: ESS
*
* McStas instrument model adapted from Vitess model by Andreas Houben
*
* %D
* McStas instrument model adapted from Vitess model
*
* Example: <parameters=values>
*
* %P
* account_vit_intensity: [1]   Apply factors to thermal intensity to accomodate different Vitess source
* bender:                [1]   Use bender for bispectral switch or not
* benderrot:             [deg] Bender orientation wrt. beamline axis
*
* %L
*
* %E
*******************************************************************************/
DEFINE INSTRUMENT ESS_DREAM(lambdamin=0.4,lambdamax=4.6,bender=1,benderrot=0.57,Xtranslation=0.03073, a1hori = 23.93, b1hori=0.0293, a1verti = 50.0,b1verti=0.1,alpha=0,cold=0.5, tx_thermal=0.022, tx_cold=-0.025, rx_cold=0.2,thres=0.003,filter=0,repeat=1,E_smear=0.1,pos_smear=0.01,dir_smear=0.01)


DECLARE
%{
  int IsCold;
  double SrcX, SrcY, SrcZ;
  double Theta;
  double MonTransl;
  double XW, YH;
  char options1[256],options2[256],options3[256],options4[256];
  char srcdef[128];
  double WidthC=0.072,WidthT=0.108;
  double WL;
  double lambdamin, lambdamax;
  double CCold,CThermal;
  double SurfSign;
  double TCollmin;
  double TCollmax;
  double Emin,Emax;
  double EminTh=20, EmaxTh=511, EminC=4, EmaxC=20;



  double Eneutron;
  /* 10 beamlines in sector N and E  - plus one location added for drawing */
  double iBeamlinesN[] = { 30.0,  36.0,  42.0,  48.0,  54.0,  60.0,  66.0,  72.0,  78.0,  84.0,  90.0};
  double iBeamlinesE[] = {-30.0, -36.0, -42.0, -48.0, -54.0, -60.0, -66.0, -72.0, -78.0, -84.0, -90.0};
  /* 11 beamlines in sector S and W - plus one location added for drawing */
  double iBeamlinesW[] = { 150.0,  144.7,  138.0,  132.7,  126.0,  120.7,  114.0,  108.7,  102.0,  96.7,  90.0,  84.0};
  double iBeamlinesS[] = {-150.0, -144.7, -138.0, -132.7, -126.0, -120.7, -114.0, -108.7, -102.0, -96.7, -90.0, -84.0};
  double* iBeamlines;
  double ANGLE;
  double DeltaX,DeltaZ;
  char MCPLfile[128];
  double T0,L0;
  char sector[10];
  int beamline=3;
  
  double calcAlpha(double length, double radius) {
    // calculate angle of arm after curved guide
    return RAD2DEG * length/radius;
  }	
  
  double calcX(double length, double radius) {
    // calculate position and angle of arm after curved guide
    double alpha = DEG2RAD * calcAlpha(length, radius);
    return radius*(1.0-cos(alpha));
  }	
  
  double calcZ(double length, double radius) {
    // calculate position and angle of arm after curved guide
    double alpha = DEG2RAD * calcAlpha(length, radius);
    return radius*sin(alpha);
  }

/* Horiz and vert. divergences*/
double VertDiv, HorDiv;
double DivFlag,VertDiv, HorDiv, VertDiv2, HorDiv2;
double guide1start;
double guide1end;
double guide2start;
double guide2end;
double guide3start;
double guide3end;
double guide4start;
double guide4end;
double guide5start;
double guide5end;

double samplepos;

double u;
double U;

double length_guide1;
double length_guide2;
double length_guide3;
double length_guide4;
double length_guide5;

double a1verti;
double a1hori;
double b1verti;
double b1hori;

double a2verti;
double a2hori;
double b2verti;
double b2hori;

double a3verti;
double a3hori;
double b3verti;
double b3hori;

double a5;
double b5;

int iter;

%include "Guide_declarations.c";


%}

INITIALIZE
%{
   

u=1e-6;
U=1e6;

guide1start=6.869;
guide1end=8.800;
guide2start=9.1358;
guide2end=13.0814;
guide3start=13.4172;
guide3end=23.491;
guide4start=23.491;
guide4end=58.691;
guide5start=guide4end+u;
guide5end=75.880;

samplepos=76.52;


length_guide1 = guide1end - guide1start;
length_guide2 = guide2end - guide2start;
length_guide3 = guide3end - guide3start;
length_guide4 = guide4end - guide4start;
length_guide5 = guide5end - guide5start;

 printf("Guide lengths are:\n guide1: %g\n guide2: %g\n guide3: %g\n guide4: %g\n guide5: %g\n", length_guide1, length_guide2, length_guide3, length_guide4, length_guide5);

a1hori = 23.75;
b1hori=0.0293;
a2hori = 23.93;
b2hori=0.0293;
a3hori = 23.93;
b3hori=0.0293;

a1verti = 17.6;
b1verti=0.0293;
a2verti = 50.0;
b2verti=0.1;
a3verti = 17.6;
b3verti=0.0293;

a5=17.6;
b5=0.0293;


 



  sprintf(sector,"S");
  sprintf(srcdef,"2015");
  if (beamline==1) {
    TCollmin=0;
    TCollmax=0.058;
  } else if (beamline==2) {
    TCollmin=0;
    TCollmax=0.06;
  }
  else {
    TCollmin=0.011;
    TCollmax=0.071;
  }
  if (strcasestr(sector,"N")) {
    iBeamlines=iBeamlinesN;
    DeltaX=-0.0585; DeltaZ=0.0925;
  } else if (strcasestr(sector,"W")) {
    iBeamlines=iBeamlinesW;
    DeltaX=0.0585; DeltaZ=0.0925;
  } else if (strcasestr(sector,"S")) {
    iBeamlines=iBeamlinesS;
    DeltaX=0.0585; DeltaZ=-0.0925;
  } else if (strcasestr(sector,"E")) {
    iBeamlines=iBeamlinesE;
    DeltaX=-0.0585; DeltaZ=-0.0925;
  }
  ANGLE=iBeamlines[beamline-1]-90;
  if (filter==0)
    sprintf(MCPLfile,"%s%i.mcpl.gz",sector,beamline);
  else
    sprintf(MCPLfile,"%s%i_filtered.mcpl.gz",sector,beamline);
  printf("MCPLfile is %s\n",MCPLfile);
%}


TRACE

COMPONENT origin = Progress_bar()
AT (0, 0, 0) RELATIVE ABSOLUTE
EXTEND %{
  DivFlag=0;
%}



/* read neutrons from an mcpl file*/

/*COMPONENT vinROT2 = Arm()
AT(0,0,0) RELATIVE PREVIOUS
  ROTATED (0,-90,0) RELATIVE PREVIOUS

COMPONENT vinROT1 = Arm()
AT(0,0,0) RELATIVE PREVIOUS
  ROTATED (-90,0,0) RELATIVE PREVIOUS*/

  COMPONENT vin = MCPL_input(filename=MCPLfile,verbose=1,repeat_count=repeat,E_smear=E_smear,pos_smear=pos_smear,dir_smear=dir_smear)
AT(0,0,0) RELATIVE PREVIOUS
EXTEND %{
  if(p>mcipthres) {ABSORB;}
  else {SCATTER;}
 p*=1.56e16;
  p/=1e5;
  z=z-0.137;
%}
 
COMPONENT Sphere1 = PSD_monitor_4PI(filename="nonrotated", radius=2.2,restore_neutron=1)
AT (0,0,0) RELATIVE PREVIOUS

/* 1. ESS_butterfly at beamport S3 accpording to https://ess-ics.atlassian.net/wiki/display/SPD/Instrument+Models */
COMPONENT ess_butterfly = ESS_butterfly(
    sector="S",
    beamline=3,
    yheight=0.03, 
    cold_frac=cold, 
    target_index=4, 
    dist=0,
    focus_xw=0.0948014, 
    focus_yh=0.04, 
    c_performance=1, 
    t_performance=1, 
    Lmin=lambdamin, 
    Lmax=lambdamax, 
    tmax_multiplier=3, 
    n_pulses=1, 
    acc_power=5)
 AT (0, 0, 0) RELATIVE PREVIOUS
ROTATED (0, alpha, 0) RELATIVE PREVIOUS
EXTEND %{
  IsCold=iscold;
  SurfSign=surf_sign;
  SrcX=x;SrcY=y;SrcZ=z;
  WL=lambda;
  Eneutron=VS2E*(vx*vx + vy*vy + vz*vz);
  if (IsCold) {
    Emin=EminC;Emax=EmaxC;
  } else {
    Emin=EminTh;Emax=EmaxTh;
  }

%}

COMPONENT Sphere0 = PSD_monitor_4PI(filename="rotated", radius=2.2,restore_neutron=1)
AT (0,0,0) RELATIVE ess_butterfly

COMPONENT Focus_cut=Shape(xwidth=0.01,yheight=0.01)
  AT(0,0,1.906) RELATIVE ess_butterfly
  ROTATED (-90, -90, 0) RELATIVE ess_butterfly
EXTEND %{
  double xtmp,ytmp,ztmp,vxtmp,vytmp,vztmp;
  xtmp=x;ytmp=y;ztmp=z;
  vxtmp=vx;vytmp=vy;vztmp=vz;
  ALLOW_BACKPROP;
  PROP_Z0;
  SCATTER;
  
  if (fabs(x)>0.06 || fabs(y)>0.06) {
    ABSORB;
  } else {
    x=xtmp;y=ytmp;z=ztmp;
    vx=vxtmp;vy=vytmp;vz=vztmp;
  }
%}

COMPONENT BackTrace = Shape(xwidth=0.3,yheight=0.3)
  AT (0,0,0.08) RELATIVE ess_butterfly
EXTEND %{
  /* Propagate back to a small rectangle in front of moderators */

  ALLOW_BACKPROP;
  PROP_Z0;
  SCATTER;
  
  /* Remove neutrons that are not from around the moderators */
  if (fabs(x)>0.12 || fabs(y)>0.03) {
   ABSORB;
 }
  WL = (2*PI/V2K)/sqrt(vx*vx + vy*vy + vz*vz);
  if (WL<mciplambdamin || WL>mciplambdamax) {
    ABSORB;
  }
  t+=rand01()*ESS_SOURCE_DURATION;
  if (t>3*ESS_SOURCE_DURATION) {
    ABSORB;
  }
  /* Measure location and energy for later use */
  SrcX=x;SrcY=y;SrcZ=z;
  Eneutron=VS2E*(vx*vx + vy*vy + vz*vz);
  if (Eneutron>EminTh) {
    Emin=EminC;Emax=EmaxC;
    IsCold=0;
  } else {
    Emin=EminTh;Emax=EmaxTh;
    IsCold=1;
  }
  T0=t;
  L0=WL;
%}

/* A "suppport" arm with x-axis along the wanted thermal guide axis */
  COMPONENT Reorient1 = Arm()
  AT (Xtranslation,0,0) RELATIVE origin
  ROTATED (0,-90,0) RELATIVE origin
  
/* The same, but keeping a z-axis along the wanted thermal guide axis */
  COMPONENT Reorient2 = Arm()
  AT (0,0,0) RELATIVE Reorient1
  ROTATED (0,0,0) RELATIVE origin
  
  /* Arm at start of guide in "normal" McStas orientation, i.e. with z along beam */
  COMPONENT Reorient3 = Arm()
  AT (0,0,1.906) RELATIVE Reorient2

  /* Arm at start of guide, shifted in x to the "centre" position of the guide width */
  /* Full width of guide is 0.0948014 m, with 0.0220029 on the thermal side and -0.0727985 on the cold side */
  COMPONENT Reorient4 = Arm()
  AT (0.0220029-(0.0948014/2.0),0,0) RELATIVE Reorient3

/* Monitors to measure the source brilliance */


/* NB: !! To show pulse-TOF "slices", put tofcuts=1 in the below monitors!! */

/* Measures "collimated" brilliance of the cold source over fixed 6 cm wide area x central part vertically. */
/* Used for calibration of performance wrt. MCNP BF1 output, see http://ess_butterfly.mcstas.org */
COMPONENT BrillmonCOLD_COLL = Brilliance_monitor(
    nlam = 101, nt = 101, filename = "brillCOLD_COLL", t_0 = -1000,
    t_1 = 4e4, lambda_0 = lambdamin, lambda_1 = lambdamax,
    Freq =14, toflambda=1,tofcuts=0, srcarea=(100*0.06*100*2*0.03/2.5), restore_neutron=1)
  WHEN((SurfSign==-1) && (IsCold) && (fabs(SrcY)<0.03/2.5) && (fabs(SrcX) < (0.071)) && (fabs(SrcX) > (0.011)))  AT (0, 0, 1) RELATIVE ess_butterfly


/* Measures "collimated" brilliance of the thermal source over fixed 6 cm wide area (or smaller at beamlines no. 1,2) x central part vertically. */
/* Used for calibration of performance wrt. MCNP BF1 output, see http://ess_butterfly.mcstas.org */
COMPONENT BrillmonTHRM_COLL = Brilliance_monitor(
    nlam = 101, nt = 101, filename = "brillTHRM_COLL", t_0 = -1000,
    t_1 =4e4, lambda_0 = lambdamin, lambda_1 = lambdamax,
      Freq =14, toflambda=1,tofcuts=0, srcarea=(100*0.06*100*2*0.03/2.5), restore_neutron=1)
  WHEN ((SurfSign==1) && (!IsCold) && (fabs(SrcY)<0.03/2.5) && (fabs(SrcX)>(TCollmin)) && (fabs(SrcX)<(TCollmax)))  AT (0, 0, 1) RELATIVE ess_butterfly


/* Measures the horizontal emmision coordinate of all neutrons - gives the "apparent width" of the moderators as seen from the beamline */
COMPONENT MonND1 = Monitor_nD(xwidth=0.25, yheight=0.06, user1=SrcX, username1="Horizontal position / [m]", 
			      options="user1 bins=201 limits=[-0.125,0.125]", restore_neutron=1)
AT (0, 0, 1) RELATIVE ess_butterfly

/* Measures the horizontal emmision coordinate of all neutrons - gives the "apparent width" of the moderators as seen from the beamline */
COMPONENT CWidth = Monitor_nD(xwidth=0.25, yheight=0.06, user1=SrcX, username1="Horizontal position / [m]", 
			      options="user1 bins=201 limits=[-0.125,0.125]", restore_neutron=1)
  WHEN(Eneutron<=EmaxC && Eneutron>=EminC) AT (0, 0, 1) RELATIVE ess_butterfly

/* Measures the horizontal emmision coordinate of all neutrons - gives the "apparent width" of the moderators as seen from the beamline */
COMPONENT TWidth = Monitor_nD(xwidth=0.25, yheight=0.06, user1=SrcX, username1="Horizontal position / [m]", 
			      options="user1 bins=201 limits=[-0.125,0.125]", restore_neutron=1)
  WHEN(Eneutron<=EmaxTh && Eneutron>=EminTh) AT (0, 0, 1) RELATIVE ess_butterfly

COMPONENT BT_init = Arm()
  AT (0, 0, 1.905) RELATIVE origin
EXTEND %{
    VertDiv = RAD2DEG*atan2(vy,vz);
    HorDiv  = RAD2DEG*atan2(vx,vz);
%}
COMPONENT BT_initC = Arm()
  AT (0, 0, 1.905) RELATIVE origin
ROTATED (0, rx_cold, 0) RELATIVE origin
EXTEND %{
  if (IsCold) {
    VertDiv = RAD2DEG*atan2(vy,vz);
    HorDiv  = RAD2DEG*atan2(vx,vz);
  }
%}




COMPONENT BT_Lambda_init_Thermal = L_monitor(xwidth=0.01, yheight=0.01, nL=100, Lmin = lambdamin, Lmax=lambdamax, filename="L_BT_in_T.dat", restore_neutron=1)
    WHEN ((!IsCold) && (fabs(VertDiv) <= 0.25) && (fabs(HorDiv) <= 0.25)) AT (tx_thermal, 0, 0.1) RELATIVE origin

COMPONENT BT_Lambda_init_Cold = L_monitor(xwidth=0.01, yheight=0.01, nL=100, Lmin = lambdamin, Lmax=lambdamax, filename="L_BT_in_C.dat", restore_neutron=1)
    WHEN ((IsCold) && (fabs(VertDiv) <= 0.25) && (fabs(HorDiv) <= 0.25)) AT (tx_cold, 0, 0.1) RELATIVE origin
  ROTATED (0, rx_cold, 0) RELATIVE origin

/* 2. 2D position monitor at beamport "Beam_port_mask.dat" */
COMPONENT BeamPortMask = PSD_monitor(
    nx=100, 
    ny=100, 
    filename="Beam_port_mask.dat",
    xwidth=0.24, 
    yheight=0.05,restore_neutron=1)
  AT (0, 0, 1.906) RELATIVE origin
  ROTATED (0,0,0) RELATIVE origin

COMPONENT BeamPortL = L_monitor(xwidth=0.24, yheight=0.05, nL=100, Lmin = lambdamin, Lmax=lambdamax, filename="L_beamport.dat", restore_neutron=1)
AT(0,0,0) RELATIVE BeamPortMask

  /* Slit at front of thermal guide, directly on new "z" axis 
COMPONENT Thermalslit = Slit(xwidth=0.044, yheight=0.023)
  AT (0,0,0) RELATIVE Reorient3
   GROUP Slits1 

  /* Slit at front of thermal guide, centred on cold guide */
  /* (coordinate found by "stepping" from centre of guide-assembly to negative extreme, add half a cold guide width 
COMPONENT Coldslit = Slit(xwidth=0.0324,yheight=0.023)
  AT (-(0.09484/2.0)+0.0324/2.0,0,0) RELATIVE Reorient4
  GROUP Slits1
  
COMPONENT Armslit = Arm()
  AT (0,0,0) RELATIVE BeamPortMask
  GROUP Slits1
EXTEND %{
  ABSORB;
  %}
*/
COMPONENT BeamPortMask2 = PSD_monitor(
    nx=100, 
    ny=100, 
    filename="Beam_port_mask2.dat",
    xwidth=0.12, 
    yheight=0.05,restore_neutron=1)
  AT (0, 0, 1.906) RELATIVE origin
  ROTATED (0,0,0) RELATIVE origin

COMPONENT Source_footprint_all_before_inpile = Monitor_nD(xwidth=0.25, yheight=0.06, user1=SrcX, user2=SrcY, username1="X at source", username2="Y at source",
					      options="user1 bins=101 limits=[-0.125 0.125] user2 bins=25 limits=[-0.03 0.03]", filename="Src_Footprint_before_inpile")
  AT (0,0,0) RELATIVE BeamPortMask2

  COMPONENT Source_footprint_all_Cold_before_inpile = Monitor_nD(xwidth=0.25, yheight=0.06, user1=SrcX, user2=SrcY, username1="X at source", username2="Y at source",
					      options="user1 bins=101 limits=[-0.125 0.125] user2 bins=25 limits=[-0.03 0.03]", filename="Src_Footprint_Cold_before_inpile")
WHEN (IsCold)  AT (0,0,0) RELATIVE BeamPortMask2

  COMPONENT Source_footprint_all_Thermal_before_inpile = Monitor_nD(xwidth=0.25, yheight=0.06, user1=SrcX, user2=SrcY, username1="X at source", username2="Y at source",
					      options="user1 bins=101 limits=[-0.125 0.125] user2 bins=25 limits=[-0.03 0.03]", filename="Src_Footprint_Thermal_before_inpile")
WHEN (!IsCold)  AT (0,0,0) RELATIVE BeamPortMask2

/*OFF-flie of NBOA*/
  COMPONENT G1=Guide_anyshape_pl(geometry="NBOA.off",xwidth=3.48)

 AT (0,0,0) RELATIVE Reorient1
  ROTATED (-90, 0, 0) RELATIVE Reorient1

  /*  GROUP InPile/*



/*COMPONENT EndSlit = Slit(xwidth=0.0296, yheight=0.0382)
  AT (0,0,0) RELATIVE EndArm*/

 /*Bridge Beam guide (BBG)‚ based on KL vitess*/
COMPONENT BBG = Guide_gravity(w1 = 0.02922,w2 = 0.02133, h1 = 0.03844,h2 = 0.03846,l = 0.421, mleft=0.5, mright=0.5, mtop=3, mbottom=3)
  AT (0, 0, 3.502) RELATIVE Reorient3

/* Arm at the end of the BBG guide */
COMPONENT EndArm = Arm()
      AT (0,0,3.923) RELATIVE Reorient3

COMPONENT EndofinpilePSD= PSD_monitor(
    nx=100, 
    ny=100, 
    filename="Endofinpile.dat",
    xwidth=0.04, 
    yheight=0.04,restore_neutron=1)
  AT (0, 0, 0) RELATIVE EndArm

COMPONENT EndofinpilePSDcold= PSD_monitor(
    nx=100, 
    ny=100, 
    filename="EndofinpileCold.dat",
    xwidth=0.04, 
    yheight=0.04,restore_neutron=1)
  WHEN (IsCold)  AT (0, 0, 0) RELATIVE EndArm

COMPONENT EndofinpilePSDthermal= PSD_monitor(
    nx=100, 
    ny=100, 
    filename="EndofinpileThermal.dat",
    xwidth=0.04, 
    yheight=0.04,restore_neutron=1)
  WHEN (!IsCold)  AT (0, 0, 0) RELATIVE EndArm


COMPONENT Source_footprint_all = Monitor_nD(xwidth=0.25, yheight=0.06, user1=SrcX, user2=SrcY, username1="X at source", username2="Y at source",
					      options="user1 bins=101 limits=[-0.125 0.125] user2 bins=25 limits=[-0.03 0.03]", filename="Src_Footprint")
  AT (0,0,0) RELATIVE EndArm

  COMPONENT Source_footprint_all_Cold = Monitor_nD(xwidth=0.25, yheight=0.06, user1=SrcX, user2=SrcY, username1="X at source", username2="Y at source",
					      options="user1 bins=101 limits=[-0.125 0.125] user2 bins=25 limits=[-0.03 0.03]", filename="Src_Footprint_Cold")
WHEN (IsCold)  AT (0,0,0) RELATIVE EndArm

  COMPONENT Source_footprint_all_Thermal = Monitor_nD(xwidth=0.25, yheight=0.06, user1=SrcX, user2=SrcY, username1="X at source", username2="Y at source",
					      options="user1 bins=101 limits=[-0.125 0.125] user2 bins=25 limits=[-0.03 0.03]", filename="Src_Footprint_Thermal")
WHEN (!IsCold)  AT (0,0,0) RELATIVE EndArm



COMPONENT EndofinpileL = L_monitor(xwidth=0.4, yheight=0.04, nL=100, Lmin = lambdamin, Lmax=lambdamax, filename="L_endofinpile.dat", restore_neutron=1)
AT(0,0,0) RELATIVE PREVIOUS


/* For measurement of Lambda-dependent BT */

COMPONENT BT_BS = Arm()
  AT (0, 0, 0) RELATIVE PREVIOUS
EXTEND %{
    VertDiv = RAD2DEG*atan2(vy,vz);
    HorDiv  = RAD2DEG*atan2(vx,vz);
%}
COMPONENT BT_Lambda_BS = L_monitor(xwidth=0.01, yheight=0.01, nL=100, Lmin = lambdamin, Lmax=lambdamax, filename="L_BT_BS.dat", restore_neutron=1)
  WHEN ((fabs(VertDiv) <= 0.25) && (fabs(HorDiv)) <= 0.25) AT (0, 0, 0) RELATIVE PREVIOUS


 /* Arm to position bispectral switch */
COMPONENT Reorient5 = Arm()
  AT (0,0,5.844) RELATIVE Reorient2

/* Beam cross-section at BS switch */
COMPONENT PSD_BS = PSD_monitor(xwidth=0.04, yheight=0.04, nx=100, ny=100, filename="PSD_BS.dat", restore_neutron=1)
AT(0,0,0) RELATIVE Reorient5

/* Horz divergence vs.  BS switch */
COMPONENT PSD_DivLambda_BS = DivLambda_monitor(nL=100, nh=100, filename="DIVL_BS.dat",
         xwidth=0.02, yheight=0.05, maxdiv_h=2, Lmin=lambdamin, Lmax=lambdamax, restore_neutron=1)
AT(0,0,0) RELATIVE Reorient5

/* Horz beam profile, WL dependent */
COMPONENT Hcross_05 = PSDlin_monitor(nx=100, filename="Horz_x_05_BS.dat", xwidth=0.05, yheight=0.05, restore_neutron=1)
  WHEN ((0.5-0.05<=WL) && (WL<=0.5+0.05)) AT(0,0,0) RELATIVE Reorient5

COMPONENT Hcross_3 = PSDlin_monitor(nx=100, filename="Horz_x_3_BS.dat", xwidth=0.05, yheight=0.05, restore_neutron=1)
  WHEN ((3-0.05<=WL) && (WL<=3+0.05)) AT(0,0,0) RELATIVE Reorient5

COMPONENT Hcross_43 = PSDlin_monitor(nx=100, filename="Horz_x_43_BS.dat", xwidth=0.05, yheight=0.05, restore_neutron=1)
  WHEN ((4.3-0.05<=WL) && (WL<=4.3+0.05)) AT(0,0,0) RELATIVE Reorient5

/* Vert beam profile, WL dependent */
COMPONENT Vcross_05 = PSDlin_monitor(nx=100, filename="Vert_y_05_BS.dat", xwidth=0.05, yheight=0.05, restore_neutron=1)
  WHEN ((0.5-0.05<=WL) && (WL<=0.5+0.05)) AT(0,0,0) RELATIVE Reorient5
  ROTATED (0,0,90) RELATIVE Reorient5

COMPONENT Vcross_3 = PSDlin_monitor(nx=100, filename="Vert_y_3_BS.dat", xwidth=0.05, yheight=0.05, restore_neutron=1)
  WHEN ((3-0.05<=WL) && (WL<=3+0.05)) AT(0,0,0) RELATIVE Reorient5
  ROTATED (0,0,90) RELATIVE Reorient5

COMPONENT Vcross_43 = PSDlin_monitor(nx=100, filename="Vert_y_43_BS.dat", xwidth=0.05, yheight=0.05, restore_neutron=1)
  WHEN ((4.3-0.05<=WL) && (WL<=4.3+0.05)) AT(0,0,0) RELATIVE Reorient5
  ROTATED (0,0,90) RELATIVE Reorient5

/* BS Switch as taken from BEER instrument */

COMPONENT Bender = Guide_multichannel(w1=0.023, h1=0.040, w2=0.023, h2=0.040, l=0.015, nslit=150, dlam=0.00015, mx=3, my=3,mater="Si") 

  AT (0,0,0) RELATIVE Reorient5
  ROTATED(0,benderrot,0) RELATIVE Reorient5

 /* Arm to position monitors AFTER bispectral switch */
COMPONENT Reorient6 = Arm()
  AT (0,0,0.02) RELATIVE Reorient5

/* For measurement of Lambda-dependent BT */

COMPONENT BT_aBS = Arm()
  AT (0, 0, 0) RELATIVE PREVIOUS
EXTEND %{
    VertDiv = RAD2DEG*atan2(vy,vz);
    HorDiv  = RAD2DEG*atan2(vx,vz);
%}
COMPONENT BT_Lambda_aBS = L_monitor(xwidth=0.01, yheight=0.01, nL=100, Lmin = lambdamin, Lmax=lambdamax, filename="L_BT_aBS.dat", restore_neutron=1)
  WHEN ((fabs(VertDiv) <= 0.25) && (fabs(HorDiv) <= 0.25)) AT (0, 0, 0) RELATIVE PREVIOUS

/* Beam cross-section after BS switch */
COMPONENT PSD_aBS = PSD_monitor(xwidth=0.04, yheight=0.04, nx=100, ny=100, filename="PSD_aBS.dat", restore_neutron=1)
AT(0,0,0) RELATIVE Reorient6

/* Beam cross-section for cold neutrons after BS switch */
COMPONENT EndofBSPSDcold= PSD_monitor(
    nx=100, 
    ny=100, 
    filename="EndofBSCold.dat",
    xwidth=0.04, 
    yheight=0.04,restore_neutron=1)
  WHEN (IsCold)  AT (0, 0, 0) RELATIVE Reorient6

COMPONENT EndofBSPSDthermal= PSD_monitor(
    nx=100, 
    ny=100, 
    filename="EndofBSThermal.dat",
    xwidth=0.04, 
    yheight=0.04,restore_neutron=1)
  WHEN (!IsCold)  AT (0, 0, 0) RELATIVE Reorient6

			 

/* Horz divergence vs.  BS switch */
COMPONENT PSD_DivLambda_aBS = DivLambda_monitor(nL=100, nh=100, filename="DIVL_aBS.dat",
         xwidth=0.02, yheight=0.05, maxdiv_h=2, Lmin=lambdamin, Lmax=lambdamax, restore_neutron=1)
AT(0,0,0) RELATIVE Reorient6

/* Horz beam profile, WL dependent */
COMPONENT Hcross_05a = PSDlin_monitor(nx=100, filename="Horz_x_05_aBS.dat", xwidth=0.05, yheight=0.05, restore_neutron=1)
  WHEN ((0.5-0.05<=WL) && (WL<=0.5+0.05)) AT(0,0,0) RELATIVE Reorient6

COMPONENT Hcross_3a = PSDlin_monitor(nx=100, filename="Horz_x_3_aBS.dat", xwidth=0.05, yheight=0.05, restore_neutron=1)
  WHEN ((3-0.05<=WL) && (WL<=3+0.05)) AT(0,0,0) RELATIVE Reorient6

COMPONENT Hcross_43a = PSDlin_monitor(nx=100, filename="Horz_x_43_aBS.dat", xwidth=0.05, yheight=0.05, restore_neutron=1)
  WHEN ((4.3-0.05<=WL) && (WL<=4.3+0.05)) AT(0,0,0) RELATIVE Reorient6

/* Vert beam profile, WL dependent */
COMPONENT Vcross_05a = PSDlin_monitor(nx=100, filename="Vert_y_05_aBS.dat", xwidth=0.05, yheight=0.05, restore_neutron=1)
  WHEN ((0.5-0.05<=WL) && (WL<=0.5+0.05)) AT(0,0,0) RELATIVE Reorient6
  ROTATED (0,0,90) RELATIVE Reorient6

COMPONENT Vcross_3a = PSDlin_monitor(nx=100, filename="Vert_y_3_aBS.dat", xwidth=0.05, yheight=0.05, restore_neutron=1)
  WHEN ((3-0.05<=WL) && (WL<=3+0.05)) AT(0,0,0) RELATIVE Reorient6
  ROTATED (0,0,90) RELATIVE Reorient6

COMPONENT Vcross_43a = PSDlin_monitor(nx=100, filename="Vert_y_43_aBS.dat", xwidth=0.05, yheight=0.05, restore_neutron=1)
  WHEN ((4.3-0.05<=WL) && (WL<=4.3+0.05)) AT(0,0,0) RELATIVE Reorient6
  ROTATED (0,0,90) RELATIVE Reorient6



COMPONENT Guide1= Elliptic_guide_gravity(
	l=length_guide1,
	majorAxisxw=a1verti,
	minorAxisxw=b1verti,
	majorAxisyh=a1hori,
	minorAxisyh=b1hori,
	majorAxisoffsetxw=length_guide3 + (guide3start-guide1start),//10.92,
	majorAxisoffsetyh=guide3end-guide1start,
	//seglength=seg_array1,
	mvaluesright=side_m_array1,mvaluesleft=side_m_array1,mvaluestop=bt_m_array1,mvaluesbottom=bt_m_array1
	)
AT (0,0, guide1start) RELATIVE Reorient2


COMPONENT Guide2= Elliptic_guide_gravity(
	l=length_guide2,
	majorAxisxw=a1verti,
	minorAxisxw=b1verti,
	majorAxisyh=a1hori,
	minorAxisyh=b1hori,
	majorAxisoffsetxw=length_guide3 + (guide3start-guide2start),//8.645,
	majorAxisoffsetyh=guide3end-guide2start,
	//seglength=seg_array2,
	mvaluesright=side_m_array2,mvaluesleft=side_m_array2,mvaluestop=bt_m_array2,mvaluesbottom=bt_m_array2
	)
AT (0,0, guide2start) RELATIVE Reorient2

COMPONENT Guide3= Elliptic_guide_gravity(
	l=length_guide3,
	majorAxisxw=a1verti,
	minorAxisxw=b1verti,
	majorAxisyh=a1hori,
	minorAxisyh=b1hori,
	majorAxisoffsetxw=length_guide3,
	majorAxisoffsetyh=length_guide3,
	//seglength=seg_array3,
	mvaluesright=side_m_array3,mvaluesleft=side_m_array3,mvaluestop=bt_m_array3,mvaluesbottom=bt_m_array3
	)
AT (0,0, guide3start) RELATIVE Reorient2

COMPONENT Guide4_1 = Guide(
		reflect="SN2.dat",                
		w1 = 0.0586,
                h1 = 0.0586,
                l = 5.5087)
  AT (0, 0, guide4start) RELATIVE Reorient2
 
 
COMPONENT Guide4_2 = Guide(
                reflect="SN1p5.dat",
		w1 = 0.0586,
                h1 = 0.0586,
                l = 24.9999)


  AT (0, 0, 5.5087) RELATIVE PREVIOUS
 
COMPONENT Guide4_3 = Guide(
                reflect="SN2.dat",
                w1 = 0.0586,
                h1 = 0.0586,
                l = 4.6912)
  AT (0, 0, 24.9999) RELATIVE PREVIOUS

 COMPONENT Guide5= Elliptic_guide_gravity(
	l=length_guide5,
	majorAxisxw=a5,
	minorAxisxw=b5,
	majorAxisyh=a5,
	minorAxisyh=b5,
	majorAxisoffsetxw=0,//0.2588,
	majorAxisoffsetyh=0,//0.2588,
	//seglength=seg_array5,
	mvaluesright=side_m_array5,mvaluesleft=side_m_array5,mvaluestop=bt_m_array5,mvaluesbottom=bt_m_array5
	)
AT (0,0, guide5start) RELATIVE Reorient2

COMPONENT Reorient7 = Arm()
AT (0, 0, samplepos) RELATIVE Reorient2


/* /\* Beam cross-section at BS switch *\/ */
/* COMPONENT PSD_ag5 = PSD_monitor(xwidth=0.02, yheight=0.05, nx=100, ny=100, filename="PSD_end_guide5.dat", restore_neutron=1) */
/* AT(0,0,0) RELATIVE Reorient7 */

/* /\* Horz divergence vs.  BS switch *\/ */
/* COMPONENT PSD_DivLambda_ag5 = DivLambda_monitor(nL=100, nh=100, filename="DIVL_end_gudie5.dat", */
/*          xwidth=0.02, yheight=0.05, maxdiv_h=2, Lmin=lambdamin, Lmax=lambdamax, restore_neutron=1) */
/* AT(0,0,0) RELATIVE Reorient7 */

/* COMPONENT BeamPortMask_ag5= PSD_monitor( */
/*     nx=100,  */
/*     ny=100,  */
/*     filename="Beam_port_mask_end_gudie5.dat", */
/*     xwidth=0.24,  */
/*     yheight=0.05,restore_neutron=1) */

/* AT(0,0,0) RELATIVE Reorient7 */



/* Sample position monitors */


COMPONENT BT_Lambda_SP = L_monitor(xwidth=0.01, yheight=0.01, nL=100, Lmin = lambdamin, Lmax=lambdamax, filename="L_BT_SP.dat", restore_neutron=1)
  WHEN ((fabs(VertDiv) <= 0.25) && (fabs(HorDiv)) <= 0.25) AT (0, 0, 0) RELATIVE PREVIOUS


/* Beam cross-section at sample position  */
COMPONENT PSD_SP1 = PSD_monitor(xwidth=0.04, yheight=0.04, nx=100, ny=100, filename="PSD_SP1.dat", restore_neutron=1)
AT(0,0,0) RELATIVE Reorient7

/* Horz divergence vs.  sample position */
COMPONENT PSD_DivLambda_SP = DivLambda_monitor(nL=100, nh=100, filename="DIVL_SP.dat",
         xwidth=0.02, yheight=0.05, maxdiv_h=2, Lmin=lambdamin, Lmax=lambdamax, restore_neutron=1)
AT(0,0,0) RELATIVE Reorient7


/* Beam cross-section for thermal neutrons */
COMPONENT PSD_th_sp = PSD_monitor(xwidth=0.01, yheight=0.01, nx=100, ny=100, filename="PSD_th_sp.dat", restore_neutron=1)
WHEN ((!IsCold) && (fabs(VertDiv) <= 0.25) && (fabs(HorDiv) <= 0.25)) AT (0, 0, 0) RELATIVE Reorient7

/* Beam cross-section for cold neutrons */
COMPONENT PSD_cold_sp = PSD_monitor(xwidth=0.01, yheight=0.01, nx=100, ny=100, filename="PSD_cold_sp.dat", restore_neutron=1)
WHEN ((IsCold) && (fabs(VertDiv) <= 0.25) && (fabs(HorDiv) <= 0.25)) AT (0, 0, 0) RELATIVE Reorient7


  
/* Horz beam profile, WL dependent */
COMPONENT Hcross_05a_SP = PSDlin_monitor(nx=100, filename="Horz_x_05_ag5.dat", xwidth=0.05, yheight=0.05, restore_neutron=1)
  WHEN ((0.5-0.05<=WL) && (WL<=0.5+0.05)) AT(0,0,samplepos) RELATIVE Reorient2

COMPONENT Hcross_3a_SP = PSDlin_monitor(nx=100, filename="Horz_x_3_ag5.dat", xwidth=0.05, yheight=0.05, restore_neutron=1)
  WHEN  ((3-0.05<=WL) && (WL<=3+0.05)) AT(0,0,samplepos) RELATIVE Reorient2

COMPONENT Hcross_43a_SP = PSDlin_monitor(nx=100, filename="Horz_x_43_ag5.dat", xwidth=0.05, yheight=0.05, restore_neutron=1)
  WHEN ((4.3-0.05<=WL) && (WL<=4.3+0.05)) AT(0,0,samplepos) RELATIVE Reorient2

/* Vert beam profile, WL dependent */
COMPONENT Vcross_05a_SP = PSDlin_monitor(nx=100, filename="Vert_y_05_ag5.dat", xwidth=0.05, yheight=0.05, restore_neutron=1)
  WHEN ((0.5-0.05<=WL) && (WL<=0.5+0.05)) AT(0,0,samplepos) RELATIVE Reorient2
  ROTATED (0,0,90) RELATIVE Reorient2

COMPONENT Vcross_3a_SP = PSDlin_monitor(nx=100, filename="Vert_y_3_ag5.dat", xwidth=0.05, yheight=0.05, restore_neutron=1)
  WHEN ((3-0.05<=WL) && (WL<=3+0.05)) AT(0,0,samplepos) RELATIVE Reorient2
  ROTATED (0,0,90) RELATIVE Reorient2

COMPONENT Vcross_43a_SP = PSDlin_monitor(nx=100, filename="Vert_y_43_ag5.dat", xwidth=0.05, yheight=0.05, restore_neutron=1)
  WHEN ((4.3-0.05<=WL) && (WL<=4.3+0.05)) AT(0,0,samplepos) RELATIVE Reorient2
  ROTATED (0,0,90) RELATIVE Reorient2
			 





COMPONENT divergence_monitor0 = Divergence_monitor(
    nh=100, 
    nv=100, 
    filename="divergence_sp_fullband", 
    xmin=-0.005, 
    xmax=0.005, 
    ymin=-0.005, 
    ymax=0.005, 
    maxdiv_h=0.5, 
    maxdiv_v=0.5,
    restore_neutron=1)
 AT (0, 0, samplepos) RELATIVE Reorient2
EXTEND %{
  if (SCATTERED) {
    DivFlag=1;
    VertDiv2 = RAD2DEG*atan2(vy,vz);
    HorDiv2  = RAD2DEG*atan2(vx,vz);
  } else {
    DivFlag=0;
  }
%}


COMPONENT divergence_monitor1 = Divergence_monitor(
    nh=100, 
    nv=100, 
    filename="div_mon_sp_05.dat", 
    xmin=-0.005, 
    xmax=0.005, 
    ymin=-0.005, 
    ymax=0.005, 
    maxdiv_h=0.5, 
    maxdiv_v=0.5,
    restore_neutron=1)
WHEN ((0.5-0.05<=WL) && (WL<=0.5+0.05))  AT (0, 0, samplepos) RELATIVE Reorient2
EXTEND %{
  if (SCATTERED) {
    DivFlag=1;
    VertDiv2 = RAD2DEG*atan2(vy,vz);
    HorDiv2  = RAD2DEG*atan2(vx,vz);
  } else {
    DivFlag=0;
  }
%}



COMPONENT divergence_monitor2 = Divergence_monitor(
    nh=100, 
    nv=100, 
    filename="div_mon_sp_1.dat", 
    xmin=-0.005, 
    xmax=0.005, 
    ymin=-0.005, 
    ymax=0.005, 
    maxdiv_h=0.5, 
    maxdiv_v=0.5,
    restore_neutron=1)
  WHEN ((1.0-0.05<=WL) && (WL<=1.0+0.05))  AT (0, 0, samplepos) RELATIVE Reorient2

COMPONENT divergence_monitor3 = Divergence_monitor(
    nh=100, 
    nv=100, 
    filename="div_mon_sp_2p8.dat", 
    xmin=-0.005, 
    xmax=0.005, 
    ymin=-0.005, 
    ymax=0.005, 
    maxdiv_h=0.5, 
    maxdiv_v=0.5,
    restore_neutron=1)
WHEN ((2.8-0.05<=WL) && (WL<=2.8+0.05))  AT (0, 0, samplepos) RELATIVE Reorient2


COMPONENT divergence_monitor4 = Divergence_monitor(
    nh=100, 
    nv=100, 
    filename="div_mon_sp_4p6.dat", 
    xmin=-0.005, 
    xmax=0.005, 
    ymin=-0.005, 
    ymax=0.005, 
    maxdiv_h=0.5, 
    maxdiv_v=0.5,
    restore_neutron=1)
WHEN ((4.6-0.05<=WL) && (WL<=4.6+0.05))  AT (0, 0, samplepos) RELATIVE Reorient2

/*PSD detectors to mimic Vitess results*/

/* Beam cross-section for all neutrons at sample position  h */
COMPONENT PSD_SP = PSD_monitor(
    nx=100,
    ny=100,
    filename="PSD_SP.dat",
    xwidth=0.04, 
    yheight=0.04,
    restore_neutron=1)
AT(0,0,samplepos) RELATIVE Reorient2


 COMPONENT psd_monitor1 = PSD_monitor(
    nx=100, 
    ny=100, 
    filename="psd_05.dat", 
    xwidth=0.04, 
    yheight=0.04, 
    restore_neutron=1)
WHEN ((DivFlag) && (0.5-0.05<=WL) && (WL<=0.5+0.05))  AT (0, 0, samplepos) RELATIVE Reorient2

COMPONENT psd_monitor2 = PSD_monitor(
    nx=100, 
    ny=100, 
    filename="psd_1.dat", 
    xwidth=0.04, 
    yheight=0.04, 
    restore_neutron=1)
WHEN ((DivFlag) && (1.0-0.05<=WL) && (WL<=1.0+0.05))  AT (0, 0, samplepos) RELATIVE Reorient2

COMPONENT psd_monitor3 = PSD_monitor(
    nx=100, 
    ny=100, 
    filename="psd_2p8.dat", 
    xwidth=0.04, 
    yheight=0.04, 
    restore_neutron=1)
WHEN ((DivFlag) && (2.8-0.05<=WL) && (WL<=2.8+0.05)) AT (0, 0, samplepos) RELATIVE Reorient2

COMPONENT psd_monitor4 = PSD_monitor(
    nx=100, 
    ny=100, 
    filename="psd_4p6.dat", 
    xwidth=0.04, 
    yheight=0.04,
    restore_neutron=1)
    WHEN ((DivFlag) && (4.6-0.05<=WL) && (WL<=4.6+0.05))  AT (0, 0, samplepos) RELATIVE Reorient2
		
FINALLY
%{
%}

END
 



