#include <fstream>
#include <iostream>
#include <vector>
#include <string>

int main(int arg, char* argv[])
{
  const std::string filename = "guide_1.txt";
  std::fstream infile(filename, std::ios::in);

  if (!infile.is_open()) {
    std::cout<<"File not found!"<<std::endl;
    return 1;
  }
  
  // Read the first two lines

  std::string aline;
  for(int iline=0; iline <2; iline++)
    getline(infile,aline);

  std::vector<double> tops;
  std::vector<double> bottoms;
  tops.clear();
  bottoms.clear();
  
  while(!infile.eof()){
    double l, w, h, top, bottom;
    std::string refl, fname, left, right; 
    infile >> l >> w >> h >> refl >> fname >> left >> right >> top >> bottom ;
    tops.push_back(top);
    bottoms.push_back(bottom);
  }

  for(size_t i=0; i< tops.size(); i++)
    std::cout << tops[i] << "\t" << bottoms[i]<<std::endl;
  
  return 0;
}
