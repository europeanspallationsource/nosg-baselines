# README #

### What is this repository for? ###

* This is the simulation baseline for the instruments
* All MCSTAS, VITESS etc code must be committed to this repository to pass TG2
* All shielding source code must be committed to this repository to pass TG3
* The engineering baseline will be compared to this simulation baseline before procurement

### What is the structure? ###

* Each instrument has its own folder, within each folder are two subfolders:
* Optics contains optics simulations, in a sub-folder by software name (MCSTAS, VITESS etc)
* Shielding contains shielding simulations, in a sub-folder by software name (MCNP, GEANT4, PHITS)
* If you are using comblayer, the comblayer repository is on github: https://github.com/SAnsell/CombLayer
* At the top level, there may be other folders not related to instruments containing universal things (e.g. source term generators, analysis scripts)
* The whole point of this is that an independent expert can reproduce your simulation data from scratch
* The master branch is protected, you will be contributing to the dev branch - let the admins know if there is a commit to merge with master

### How do I get set up? ###

* Issue the following command:
```
git clone git@bitbucket.org:europeanspallationsource/nosg-baselines.git
```
* Note that the LOKI folder is a subtree, with `prefix=LOKI/Optics/McStas/` and remote origin set to `git@bitbucket.org:essloki/loki-mcstas-master.git`
* Any other projects who are following NOSG standards and already have a version control history should talk to the admins below about adding their history as a subtree, so that the commits are not lost

### How do I commit changes? ###
* Before you work, issue a `git pull origin dev` command to refresh the repository to the latest state.  If you work with the repository as it is, you will automatically get all simulation geometries from all instruments
* Feel free to create whatever branches you want on your local machine - origin master is for the official instrument baselines and is protected
* It would be preferred that you push a branch to origin that is clearly and appropriately named
* You can work directly on the dev branch, and merge your own branches onto dev.
* Pushing branches to origin without being asked to do so might result in them being culled.
* Before you push to origin/dev, do `git pull origin dev` in case someone else has pushed a commit whilst you were working.
* Now do `git push origin dev`

### Contribution guidelines ###

* Before you push to origin/dev, make certain that you are done-done, and that your code compiles and works properly
* Make certain that your code runs with no flags / options needing to be set.
* Only commit source code
* Do not commit output data!
* Do not commit source term data!
* If your model requires a source term, document it in a text file in the folder, and state the location of the simulations of the source term(s) that you used to generate the source term data
* It's OK to add a commit of a few 100 kB png figure of the results before you push, once the work is complete.  Actually, this is useful.
* Do not commit word documents!  Text files (.txt) are preferred, but take care that your text editor does not unwrap text and spam microsoft windows carriage returns everywhere.
* Even though git creates a log, to make things easier please add your name to the version history log in the folder you worked on

### Who do I talk to? ###

* Phillip Bentley <phillip.bentley@esss.se> about this repository
* Andrew Jackson <andrew.jackson@esss.se> who maintains the subtree for LOKI

### Acknowledgements ###

Other than the instrument teams, who obviously do the hard work contained in this repository(!) there were a few people who
contributed to setting this up behind the scenes:

* Kalliopi Kanaki
* Damian Martin Rodriguez
* Douglas DiJulio
