# Using LoKI Instrument Files in McSTAS #

This repository folder contains the instrument files used for simulating the LoKI SANS instrument using McSTAS.

## Release Notes ##
- 2017-03-20 : Updated to use width and height for apertures rather than radius. Height set to be equal to width unless larger than guide height, then set to be guide height.
- 2017-03-20 : Removed command line option for guide width.
- 2017-03-17 : Fixed front end geometry to match engineering geometry
- 2017-02-08 : Corrected chopper settings and made guide the same dimensions throughout
- 2017-01-27 : Updated to use new butterfly moderator

## Input Variables ##
 **collen**: [m] collimation length [2.0|5.0|8.0|10.0]  
 **reardet**: [m] position of rear detector relative to sample  
 **l_min**: [Å] minimum wavelength from full frame (used to calculate chopper openings)  
 **l_max**: [Å] maximum wavelength from full frame (used to calculate chopper openings)  
 **sourceapx**: [m] width of the source aperture  
 **sampleapx**: [m] width of the sample aperture  
 **pulseskip**: [none] number of pulses to skip  
 **modheight**: [m] height of moderator  
 **sampletype**: [integer] 0 = water, >0 select model from sans_benchmark component  

The defaults for these variables vary from file to file, so if you want specific values, make sure to set them at the command line when you run McSTAS.
The wavelength range fed into the simulation is autocalculated to be sufficiently wide that the choppers will define the wavelength band, but not so wide as to have wasted neutron trajectories.

## Usage ##
The instrument files can be run either via the `mcrun` command or converted to C using `mcstas` and then compiled with `gcc` or `mpicc`.

### Example 1 ###
LoKI on the 3cm high pancake moderator, with collimation length of 8 m and matched sample-to-detector distance, running at 14Hz and filling the frame with no frame overlap, with scattering from spheres on 200 A radius.

`mcrun loki-master-model.instr collen=10.0 reardet=8.0 l_min = 3.0 l_max=10.0 sourceaprx=0.02 sampleapx=0.01 pulseskip=0 modheight=0.03 sampletype=13
`

### Example 2 ###
LoKI on the 3cm high pancake moderator, with collimation length of 3 m and sample-to-detector distance of 5 m, running at 7Hz and filling the frame with no frame overlap, with scattering from water.

`mcrun loki_master_fullframe.instr collen=3.0 reardet=5.0 l_min = 3.0 l_max=21.4 sourceapx=0.02 sampleapx=0.01 pulseskip=1 modheight=0.03 sampletype=0
`
### Values for `L_Min` and `L_Max` ###

To calculate the correct values of `l_min` and `l_max` for frame filling you need to edit and run the `chopper-settings-for-mcstas.py` found in the same folder as the model files in the repository to calculate it. Or use the "recommended" values below (calculated using the above mentioned python script).

* 14Hz, rear detector at 33.5 m (sdd = 10 m) :
	*  `l_min = 3.0`
	*   `l_max = 10.0`  
* 14Hz, rear detector at 28.5 m (sdd = 5 m):
	*  `l_min = 3.0`
	*   `l_max = 11.5`  
* 7Hz, rear detector at 33.5 m (sdd = 10 m) :
	*  `l_min = 3.0`
	*   `l_max = 18.0`   
* 7Hz, rear detector at 28.5 m (sdd = 5 m):
	*  `l_min = 3.0`
	*   `l_max = 21.4`  

Test parameters can also be found at https://confluence.esss.lu.se/display/loki/Detector+Testing+via+Simulations

## Future Improvements ##

* Provide a "frame filling" option that calculates chopper settings for any given sample-to-detector distance
