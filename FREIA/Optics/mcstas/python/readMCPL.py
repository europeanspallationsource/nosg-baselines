#!/usr/bin/env python3

import mcpl
import sys
import math as m
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.pyplot import cm



###### constants ###############

h_Planck = 4.13566e-12 # MeV*ns
c_light = 2.99792458e8 # m/s;
neutron_mass_c2 = 939.56536 # MeV;


###################################
# conversiion from MeV to Angstrom 
def neutronEKinToWavelength(e): # e in MeV
    if (e==0):
        return float("inf")
    else:
        return 10*h_Planck*c_light*m.sqrt(0.5/(neutron_mass_c2*e)); # in Angstrom

##################################
# calculate transmission from a list of binned wavelengths (or any other propetry)
# without windows and a list with windows on
def calcTransmission(before, after): # lists as input
    a = np.array(before)
    b = np.array(after)
    return 100*(1-(a-b)/a) # TODO, treat division with 0 to return NaN

#################################

marker='o'
markersize=2

fig, ax = plt.subplots()
ax.set_xlabel('$\lambda$ ($\AA$)')
ax.set_ylabel('Average intensity (n/s)')

files = [ 'myoutput_afterWBC1.mcpl'
          ,'myoutput_afterPSCh3.mcpl'
          ,'myoutput_afterPSCh4.mcpl'
          ,'myoutput_afterWBC3.mcpl'
          ,'myoutput_guideexit.mcpl'
          ,'myoutput_presample.mcpl'
]

legends = []

for f in files:
    legends.append(str(f[9:-5]))

####################################
# function to return counts and wavelength bins after looping inside the directory files
def analyseFiles(directory, files, ax, ls='-'):
    
    plt.gca().set_prop_cycle(None) # reset the color cycle
    hists = []
    for i, f in enumerate(files):
        fi = mcpl.MCPLFile(directory+'/'+f)
        wavel, w = [],[]
        #time = []
        for index, p in enumerate(fi.particles):
            #print( p.pdgcode, p.x, p.y, p.z, p.ux, p.uy, p.uz, p.ekin, p.time, p.weight )
            wavelength = neutronEKinToWavelength(p.ekin) # from MeV to Angstrom
            #ekin.append(p.ekin*1e9) # from MeV to meV
            wavel.append(wavelength) 
            w.append(p.weight)
            #time.append(p.time) # tof in ms
            #if(index>int(sys.argv[1])): # uncomment for looping over a subset of particles
            #    break
        counts, edges = np.histogram(wavel, weights=w, bins=60, range=[2,12])
        #countsT, binsT = np.histogram(time, weights=w, bins=200)#, range=[1,12])
        hists.append(counts)
        bins = 0.5*(edges[1:]+edges[:-1])
        ax.errorbar(bins, counts, yerr=np.sqrt(counts), marker=marker, markersize=markersize, linestyle=ls, lw=0.5)
        #axT.errorbar(binsT[:-1], countsT, yerr=np.sqrt(countsT))
        
    ax.legend(legends, frameon=False)
    return hists, bins
    

    
##################################################################################


h_nowin, bins_nowin = analyseFiles('mon_noWFM_nowindows', files, ax)
h_win,   bins_win   = analyseFiles('mon_noWFM_windows', files, ax, ls='--')

# order of BMs: WBC1, PSCh3, PSCh4, WBC3, guide exit, presample
distances = [6.8, 8.8, 16, 17.3, 22.4, 26] # TODO, correct distances from source


integrals_nowin = []
integrals_win = []

for h in h_nowin:
    integrals_nowin.append(sum(h))
for h in h_win:
    integrals_win.append(sum(h))


fig1, ax1 = plt.subplots()
ax1.set_xlabel('Distance from source (m)')
ax1.set_ylabel('Average intensity (n/s)')
ax1.grid()

assert len(distances)==len(integrals_nowin), 'Size of distances and integrals is different.'


ax1.plot(distances, integrals_nowin, 'bo', ls='--', lw=0.5)
ax1.plot(distances, integrals_win, 'ro', ls='--', lw=0.5)
ax1.legend(['w/o windows','w windows'], frameon=False)

figD, axD = plt.subplots()
axD.set_xlabel('$\lambda$ ($\AA$)')
axD.set_ylabel('Transmission (%)')
axD.grid()

for i in range(len(h_nowin)):
    transmission = calcTransmission(h_nowin[i], h_win[i])
    axD.errorbar(bins_nowin, transmission, yerr=None, marker=marker, markersize=markersize, lw=0.5)

axD.legend(legends, frameon=False)
    


plt.show()
